import {isEmptyValue} from "@shared/tools";
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";

export function onSelectFilter(self, selectkey: { handler: string }) {
    if (selectkey) {
        if (!isEmptyValue(selectkey)) {
            self[selectkey.handler](selectkey);
        } else {
            self[selectkey.handler]()
        }
    } else {
        self[selectkey.handler]();
    }
}

export function getDataSelect(table) {
    const request: RequestObject = <RequestObject>{
        uri: table + "/",
        params: {},
        method: ConstanteWs._CODE_GET
    };
    return new Promise<[]>((resolve, reject) => {
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    resolve(response.payload);
                } else {
                    console.error(
                        `Error in /getDataSelect, error code :: ${response.code}`
                    );
                    this.toast.error();
                    reject([]);
                }
            },
            error: (error) => {
                console.error(
                    `Error in /getDataSelect, error :: ${error}`
                );
                this.toast.error();
                reject([]);

            }
        });


    });

}

