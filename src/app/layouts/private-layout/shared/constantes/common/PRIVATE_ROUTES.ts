import { Routes } from '@angular/router';
import {GestionUsersModule} from "@privateLayout/features/gestion-users/gestion-users.module";
export const PRIVATE_ROUTES: Routes = [

  {
    path: 'adm',
    data: {
      title: 'adm.admin',
      breadcrumb: 'adm.admin',
    },
    loadChildren: () => import('../../../features/gestion-users/gestion-users.module').then((m) => m.GestionUsersModule)
  },
  {
    path: 'gen',
    data: {
      title: "Génerateur d'application",
      breadcrumb: 'Génerateur d\'application',
    },
    loadChildren: () => import('../../../features/generateur/generateur.module').then((m) => m.GenerateurModule)
  },







]
