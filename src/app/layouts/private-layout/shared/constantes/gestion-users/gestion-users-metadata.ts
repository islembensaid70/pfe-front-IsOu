import {Icons} from "@shared/constantes/Icons";

export const usersTable = {
    ref: "TableUsers",
    title: "Liste des Utlisateurs",
    hasAdd: true,
    hasExport: false,
    hasFilter: true,
    columns: [
        {
            key: "login",
            label: "adm.gu.content.tableGu.login",
            type: "",
            style: ""
        },

        {
            label: "adm.gu.content.tableGu.nom",
            key: "nom",

        },
        {
            label: "adm.gu.content.tableGu.prenom",
            key: "prenom",

        },
        {
            label: "",
            key: "actions",
            type: "actions", btns: [
                Icons.details,
                Icons.edit
            ]
        }
    ]
}
