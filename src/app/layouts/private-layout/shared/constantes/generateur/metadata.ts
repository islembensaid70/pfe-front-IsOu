import { Icons } from "@shared/constantes/Icons";
import { SelectMetadata } from "@shared/models";
import {COMMON_Filter_TYPES, COMMON_TYPES_CODES} from "@shared/constantes/Constante";

export const tableProjet = {
  ref: "TableProjet",
  title: "Table des Projets",
  hasAdd: false, // true | false, default: true
  hasExport: false, // true | false, default: true
  columns: [
    {
      label: "Nom du Projet",
      key: "nomProjet",
      style: {
        "text-align": "center"

      }

    },

    {
      label: "Nom du partie Backend",
      key: "nomAppB",
      style: {
        "text-align": "center"

      }

    },
    {
      label: "Nom du partie Frontend",
      key: "nomAppF",
      style: {
        "text-align": "center"

      }

    },
    {
      label: "",
      key: "actions",
      type: "actions",
      style: {
        "text-align": "left"

      },
      sortable: false,
      btns: [

        Icons.details
      ]
    }


  ]
};




export const selectedLangue = <SelectMetadata>{
  label: "Langue",
  optionLabel: {
    ar: "labelLang",
    fr: "labelLang",
    en: "labelLang"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "keyLang"
};

export const typeCol = <SelectMetadata>{
  label: "Type du colonne",
  optionLabel: {
    ar: "key",
    fr: "key",
    en: "key"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "value"
};

export  const iconsMetadata= <SelectMetadata>{
  label: "Icon",
  optionLabel: {
    ar: "keyIcon",
    fr: "keyIcon",
    en: "keyIcon"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "valueIcon",


}

export const selectedLangues = <SelectMetadata>{
  label: "Langue",
  optionLabel: {
    ar: "labelLang",
    fr: "labelLang",
    en: "labelLang"


  },
  muliple:true,
  filter: true,
  tooltip: true,
  reset: false,

  value: "idLang",

};

export const profil = <SelectMetadata>{
  label: "Profils de la partie serveur",
  optionLabel: {
    ar: "labelProfile",
    fr: "labelProfile",
    en: "labelProfile"

  },
  muliple:true,
  filter: true,
  tooltip: true,
  reset: false,
  value: "idProfile",
  emitedValue: "—",


};



export const selectedMs = <SelectMetadata>{
  label: "Micro Service",
  optionLabel: {
    ar: "nomMs",
    fr: "nomMs",
    en: "nomMs"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "pathMs",
  emitedValue: "—"
};
export const    selectedTab = <SelectMetadata>{
  label: "Table",
  optionLabel: {
    ar: "className",
    fr: "className",
    en: "className"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "",
  disabled: true,
  emitedValue: "—"

};

export const selectedOrient = <SelectMetadata>{
  label: "Orientation",
  optionLabel: {
    ar: "ex",
    fr: "ex",
    en: "ex"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "ex"
};


export const entityMetadata = <SelectMetadata>{
  label: "Attributs",
  optionLabel: {
    ar: "att",
    fr: "att",
    en: "att"


  },
  filter: false,
  tooltip: true,
  reset: false,
  value: "att"

};

export const controllerDetail= <SelectMetadata>{
  label: "Controlleurs",
  optionLabel: {
    ar: "className",
    fr: "className",
    en: "className"


  },
  filter: true,
  tooltip: true,
  reset: false,
  value: null,
  emitedValue:"—"

}

export const controllerMappingDetail= <SelectMetadata>{
  label: "Controlleurs",
  optionLabel: {
    ar: "valueController",
    fr: "valueController",
    en: "valueController"


  },
  filter: true,
  tooltip: true,
  reset: false,
  value: "valueController"

};
export const filtreProjetGen={
  ref:'FilreProjet',
  title:"Filtrer vos Projets",
  fields:[{
    label:"Nom du Projet",
    key:"nomProjet",
    type: COMMON_Filter_TYPES.Text,


  },
  {
      label:"Nom du partie Backend",
      key:"nomAppB",
      type: COMMON_Filter_TYPES.Text,


    },
    {
      label:"Nom du partie Frontend",
      key:"nomAppF",
      type: COMMON_Filter_TYPES.Text,


    }

    //
    // {
    //   label:"Test Select couleur primaire",
    //   key:"keyLang",
    //   table:"tNmLangue",
    //   type: COMMON_Filter_TYPES.SELECT,
    //   selectMetaData:{
    //
    //     label: "Les couleurs ",
    //     optionLabel: {
    //       ar: "labelLang",
    //       fr: "labelLang",
    //       en: "labelLang"
    //
    //
    //     },
    //     filter: false,
    //     tooltip: false,
    //     reset: true,
    //     value: "keyLang"
    //
    //   }
    //
    //
    // },
    //
    // {
    //   label:"Test Select couleur primaire",
    //   key:"idProfile",
    //   table:"tProfile",
    //   type: COMMON_Filter_TYPES.SELECT,
    //   selectMetaData:{
    //
    //     label: "Les couleurs ",
    //     optionLabel: {
    //       ar: "idProfile",
    //       fr: "idProfile",
    //       en: "idProfile"
    //
    //
    //     },
    //     filter: false,
    //     tooltip: false,
    //     reset: true,
    //     value: "idProfile"
    //
    //   }
    //
    //
    // }

  ]

}


export const filtreTypesMetadata={
  label: "Type du Colonne ",
  optionLabel: {
    ar: "key",
    fr: "key",
    en: "key"
  },
  filter: false,
  tooltip: false,
  reset: true,
  value: "key"

}
export const filtreSpeMetadata={
  label: "Criteria  Colonne ",
  optionLabel: {
    ar: "key",
    fr: "key",
    en: "key"
  },
  filter: false,
  tooltip: false,
  reset: true,
  value: "key"

}

