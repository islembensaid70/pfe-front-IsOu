import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

@Component({
    selector: 'app-test-ouss',
    templateUrl: './test-ouss.component.html',
    styleUrls: ['./test-ouss.component.css']
})
export class TestOussComponent implements OnInit {
    displayedColumns: any;
    responsePayload: any = { "code": "200", "payload": [ { "flgActif": true, "dtMaj": "2023-01-24 00:00:00.0", "dtAjout": "2023-01-24 00:00:00.0", "role": "Administrateur", "code": "ADMIN", "libelleEn": "Administrateur", "ordre": 1, "idAdmApplication": 1, "id": 1, "libelleFr": "Administrateur", "libelleAr": "ادارة المنظومة", "isChecked": false }, { "role": "Suivi des actions entreprises pour la résolution des problèmes des citoyens", "code": "RS", "ordre": 4, "libelleFr": "Responsable du suivi", "libelleAr": "مسؤول المتابعة", "idCateg": 4, "isChecked": false, "flgActif": true, "dtMaj": "2023-01-18 00:00:00.0", "dtAjout": "2023-01-18 00:00:00.0", "libelleEn": "Responsable du suivi", "idAdmApplication": 1, "id": 120 }, { "role": "Réception et affectation des requêtes", "code": "INST", "ordre": 3, "libelleFr": "Instructeur", "libelleAr": "صاحب التعليمات", "idCateg": 3, "isChecked": false, "flgActif": true, "dtMaj": "2023-01-18 00:00:00.0", "dtAjout": "2023-01-18 00:00:00.0", "libelleEn": "Instructeur", "idAdmApplication": 1, "id": 122 }, { "role": "Consultation des statistiques et tableaux de bord\r\n", "code": "STB", "ordre": 5, "libelleFr": "Statistiques et tableaux de bord", "libelleAr": "الإحصائيات ولوحات المعلومات", "idCateg": 5, "isChecked": false, "flgActif": true, "dtMaj": "2023-01-18 00:00:00.0", "dtAjout": "2023-01-18 00:00:00.0", "libelleEn": "Statistiques et tableaux de bord\r\n", "idAdmApplication": 1, "id": 121 }, { "role": "Saisie des requêtes", "code": "GU", "ordre": 2, "libelleFr": "Guichet unique", "libelleAr": "شباك موحد", "idCateg": 2, "isChecked": false, "flgActif": true, "dtMaj": "2022-12-16 00:00:00.0", "dtAjout": "2022-12-16 00:00:00.0", "libelleEn": "Guichet unique", "idAdmApplication": 1, "id": 2 } ] };
    formUser: any;

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.displayedColumns = ['libelleFr', 'role', 'code', 'dtMaj', 'delete', 'update']
        this.formUser = this.formBuilder.group({
            libelleFr: this.formBuilder.control('', Validators.required),
            role: this.formBuilder.control('', Validators.required),
            code: this.formBuilder.control('', Validators.required),

        })
    }


    delete(element) {


    }

    update(element) {

    }


    onSubmit() {

    }
}
