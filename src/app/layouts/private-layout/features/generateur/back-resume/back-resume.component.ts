import {Component, OnInit} from '@angular/core';
import {Pagination, RequestObject, SearchObject} from "@shared/models";
import {onAction} from "@shared/tools";
import {tableProjet} from "@privateLayout/shared/constantes/generateur/metadata";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {ActivatedRoute, Router} from "@angular/router";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";

@Component({
    selector: 'app-back-resume',
    templateUrl: './back-resume.component.html',
    styleUrls: ['./back-resume.component.css']
})
export class BackResumeComponent implements OnInit {
    onAction = onAction;
    searchObject: SearchObject;
    responsePayload: any = [];
    metadataMsProjet: any = {};
    idProjet: number;

    constructor(private generateurService: GenerateurService,
                private sharedService: SharedService,
                private toast: ToastService, private router: Router,
                private activatedRoute: ActivatedRoute) {
        this.idProjet = this.activatedRoute.snapshot.params.id;
        console.log("idd",this.idProjet)
    }

    ngOnInit(): void {
        this.searchObject = this.initSearchObject();
        this.initDataProjetMs();
    }

    initSearchObject() {
        const searchObject = new SearchObject();
        searchObject.pagination = new Pagination(0, 10);

        console.log(searchObject);
        return searchObject;
    }

    initParams() {
        this.metadataMsProjet = tableProjet;

    }

    private initDataProjetMs() {
        const requestt: RequestObject = <RequestObject>{
            uri: "tMicroservice/prj/",
            params: {
                path: [this.idProjet]
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(requestt).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    this.responsePayload = response.payload;


                } else {
                    console.error(
                        `Error in FicheHomeComponent/initDataProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in FicheHomeComponent/initDataProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });

    }
}
