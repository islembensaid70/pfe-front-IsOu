import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {GenerateurRoutingModule} from './generateur-routing.module';
import {GestionFrontComponent} from './gestion-front/gestion-front.component';
import {HomeComponent} from './home/home.component';
import {ProjetComponent} from './projet/projet.component';
import {SharedModule} from "@shared/shared.module";
import {ColorBlockModule} from 'ngx-color/block';
import {ColorSketchModule} from "ngx-color/sketch";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";
import {MatTreeModule} from "@angular/material/tree";
import {DetailFrontComponent} from './detail-front/detail-front.component';
import {ModuleComponent} from './module/module.component';
import {FicheDataTableComponent} from './fiche-data-table/fiche-data-table.component';
import {ColumnDetailComponent} from './fiche-data-table/column-detail/column-detail.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FileViewerComponent} from './detail-front/file-viewer/file-viewer.component';
import {GestionBackComponent} from "@privateLayout/features/generateur/gestion-back/gestion-back.component";
import {
    DialogEditMsComponent
} from "@privateLayout/features/generateur/gestion-back/dialog-edit-ms/dialog-edit-ms.component";
import {DialogMSComponent} from "@privateLayout/features/generateur/gestion-back/dialog-ms/dialog-ms.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatStepperModule} from "@angular/material/stepper";
import {
    GestionBackReverseComponent
} from "@privateLayout/features/generateur/gestion-back-reverse/gestion-back-reverse.component";
import {BackResumeComponent} from './back-resume/back-resume.component';
import {AppRunComponent} from './app-run/app-run.component';
import {MatSortModule} from "@angular/material/sort";
import {TestOussComponent} from './test-ouss/test-ouss.component';
import {FiltreColumnComponent} from './fiche-data-table/filtre-column/filtre-column.component';
import {FicheDetailComponent} from "@privateLayout/features/generateur/fiche-detail/fiche-detail.component";
import {
    ColumnFicheDetailComponent
} from "@privateLayout/features/generateur/fiche-detail/column-fiche-detail/column-fiche-detail.component";


@NgModule({
    declarations: [
        HomeComponent,
        ProjetComponent,
        GestionFrontComponent,
        DetailFrontComponent,
        ModuleComponent,
        FicheDataTableComponent,
        ColumnDetailComponent,
        FileViewerComponent,
        GestionBackComponent,
        DialogMSComponent,
        DialogEditMsComponent,
        GestionBackReverseComponent,
        BackResumeComponent,
        AppRunComponent,
        TestOussComponent,
        FiltreColumnComponent,
        FicheDetailComponent,
        ColumnFicheDetailComponent
    ],
    imports: [
        CommonModule,
        GenerateurRoutingModule,
        SharedModule,
        ColorBlockModule,
        ColorSketchModule,
        MatTreeModule,
        MatSlideToggleModule,
        MatExpansionModule,
        MatStepperModule,
        MatSortModule
    ],
    providers: [GenerateurService]
})
export class GenerateurModule {
}
