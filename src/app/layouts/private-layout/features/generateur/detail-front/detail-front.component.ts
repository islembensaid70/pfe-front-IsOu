import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, UntypedFormControl, Validators} from "@angular/forms";
import {MatTree, MatTreeFlatDataSource, MatTreeNestedDataSource} from "@angular/material/tree";
import {FlatTreeControl, NestedTreeControl} from "@angular/cdk/tree";
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {ModuleComponent} from "@privateLayout/features/generateur/module/module.component";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {async} from "rxjs";

interface FileNode {
    name: string;
    type: string;
    module: string;
    children?: FileNode[];
    parent: string,
}


@Component({
    selector: "app-detail-front",
    templateUrl: "./detail-front.component.html",
    styleUrls: ["./detail-front.component.css"]
})
export class DetailFrontComponent implements OnInit, AfterViewInit {
    formM: FormGroup;

    responsePayload: any = {};

    idProjet: number;
    @ViewChild("fileFrame") fileFrame: ElementRef;

    dataSource = new MatTreeNestedDataSource<FileNode>();
    dataSourceShow = new MatTreeNestedDataSource<FileNode>();

    treeControl = new NestedTreeControl<FileNode>(node => node.children);

    @ViewChild(MatTree) tree: MatTree<any>;
    node: any;
    contentFile: any;
    selectedNode: FileNode;
    projet: any;

    constructor(private router: Router, private activatedRoute: ActivatedRoute,
                private sharedService: SharedService,
                private toast: ToastService,
                private _formBuilder: FormBuilder,
                private generateurService: GenerateurService,
                private sanitizer: DomSanitizer) {
        this.idProjet = this.activatedRoute.snapshot.params.idProjet;
    }

    ngOnInit() {

        this.initArchProjet();
        this.getProjet();


    }


    openDialog(nodee) {


        console.log("selected node info from detail");

        console.log(nodee);


        this.sharedService.openDialog(ModuleComponent
            , {
                idProjet: this.idProjet, node: nodee,
                treeControl: this.treeControl,
                dataSource: this.dataSource,
                index: 0
            }).subscribe(
            async (response) => {

                if (response) {

                    this.initArchProjet();
                    console.log("terminated");

                }

            });

    }


    findNodeByName(nodes: FileNode[], name: string, parent: string, index: number = 0): number | null {
        for (let i = 0; i < nodes.length; i++) {
            const nodeIndex = index + i;
            if (nodes[i].name === name && nodes[i].parent === parent) {
                return nodeIndex;
            }

            if (nodes[i].children) {
                const childIndex = this.findNodeByName(nodes[i].children, name, parent, nodeIndex + 1);
                if (childIndex !== null) {
                    return childIndex;
                }
            }
        }

        return null;
    }


    ngAfterViewInit() {
        // this.treeControl.expandAll()


    }

    initArchProjet() {
        const request: RequestObject = <RequestObject><unknown>{
            uri: "tProjet/getArch/",
            params: {
                path: [this.idProjet]
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.responsePayload = response.payload;


                    this.dataSource.data = this.responsePayload;
                    console.log("dataa projet arch", this.responsePayload);
                    this.dataSourceShow.data = this.dataSource.data.filter((node) => node.type == "dir");
                    console.log("dataasource", this.dataSourceShow.data);
                    this.treeControl.dataNodes = this.dataSource.data;
                    this.treeControl.expand(this.treeControl.dataNodes[5])


                } else {
                    console.error(
                        `Error in ModuleComponent/initArchProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/initArchProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });


    }

    onSelectionChanged(node) {
        this.selectedNode = node;
    }


    getProjet() {
        const request: RequestObject = <RequestObject><unknown>{
            uri: "tProjet/",
            params: {
                path: [this.idProjet]
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.projet = response.payload;
                    console.log("projet", this.projet);


                } else {
                    console.error(
                        `Error in ModuleComponent/getProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/getProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });


    }

    hasChild = (_: number, node: FileNode) => !!node.children && node.children.length > 0;
    btnClicked = false;

    checkType(node): boolean {

        return node.type === "dir";

    }


    goToComp(node) {
        const data = {
            node: node,
            idProjet: this.idProjet
        };

        this.generateurService.node = node;

        const navigationExtras: NavigationExtras = {
            queryParams: {data: JSON.stringify(data)}
        };

        // this.router.navigate(['/app/gen/dataTable'],navigationExtras)

        this.router.navigate(["/app/gen/dataTable", this.idProjet]);

    }

    onNodeSelected(node) {
        console.log("node", node);

        //
        // const request: RequestObject = <RequestObject><unknown>{
        //   uri: "tProjet/getContentFile/",
        //   params: {
        //     body: { name:node['name'],
        //     parent:node['parent']}
        //   },
        //   method: ConstanteWs._CODE_POST
        // };
        // this.sharedService.commonWs(request).subscribe({
        //   next: (response) => {
        //     if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
        //
        //     console.log("cd",response)
        //
        //     } else {
        //       console.error(
        //         `Error in ModuleComponent/initArchProjet, error code :: ${response.code}`
        //       );
        //       this.toast.error();
        //     }
        //   },
        //   error: (error) => {
        //     console.error(
        //       `Error in ModuleComponent/initArchProjet, error :: ${error}`
        //     );
        //     this.toast.error();
        //   }
        // });


    }

    showFile(node) {
        // this.router.navigate(['app/gen/file'])
        this.btnClicked = !this.btnClicked;

        const request: RequestObject = <RequestObject><unknown>{
            uri: "tProjet/getContentFile/",
            params: {
                body: {
                    name: node["name"],
                    parent: node["parent"]
                }
            },
            method: ConstanteWs._CODE_POST
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    this.contentFile = response;

                    console.log("cd", response);

                } else {
                    console.error(
                        `Error in ModuleComponent/initArchProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/initArchProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });
        return this.contentFile;
    }

    expand(node) {
        this.treeControl.dataNodes = this.dataSource.data;
        const index = this.findNodeByName(this.dataSource.data, node.name, node.parent);
        console.log("index", index);
        this.treeControl.expand(node);
    }

    returnicon(node) {
        if (node.name.endsWith(".html")) {
            return "html";
        } else if (node.name.endsWith(".ts")) {
            return "ts";
        } else if (node.name.endsWith(".css") || node.name.endsWith(".scss"))
            return "css";
        else
            return "description";

    }

    getNodeIndexOnClick(event: MouseEvent, rootNode: HTMLElement, treeData: FileNode): number | null {
        // Step 2: Get the clicked DOM element
        const clickedElement = event.target as HTMLElement;

        // Step 3: Traverse the DOM hierarchy to find the corresponding FileNode object
        let currentNode = clickedElement;
        while (currentNode && currentNode !== rootNode) {
            const nodeIndex = currentNode.dataset.index;
            if (nodeIndex) {
                const index = parseInt(nodeIndex, 10);
                return index;
            }
            currentNode = currentNode.parentNode as HTMLElement;
        }

        // Step 4: Return the node index
        return null;
    }

    runApp() {
        console.log("runn");

        const request: RequestObject = <RequestObject><unknown>{
            uri: "tProjet/runFront",
            params: {
                body: {
                    idProjet: this.projet['idProjet'],
                    nomProjet: this.projet['nomProjet'],
                    emplProjet: this.projet['emplProjet'],
                    nomAppB: this.projet['nomAppB'],
                    nomAppF: this.projet['nomAppF'],
                    couleurPrim: this.projet['couleurPrim'],
                    couleurSideB: this.projet['couleurSideB'],
                    couleurNavB: this.projet['couleurNavB'],
                    logo: this.projet['logo']


                }
            },
            method: ConstanteWs._CODE_POST
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    console.log("cd", response);
                    // this.router.navigate(['/app/gen/app-run'])


                } else {
                    console.error(
                        `Error in DetailComponent/initArchProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/initArchProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });

    }

    icon(node) {
        if(node.name.endsWith('html'))
            return "html"
        else if (node.name.endsWith('css'))
            return "css"
        else if(node.name.endsWith('component.ts'))
            return "tsComp"
        else if(node.name.endsWith('routing.module.ts'))
            return "routeM"
        else if(node.name.endsWith('.module.ts'))
            return "module"




    }

    goToCompDetail(node) {
        const data = {
            node: node,
            idProjet: this.idProjet
        };

        this.generateurService.node = node;

        const navigationExtras: NavigationExtras = {
            queryParams: {data: JSON.stringify(data)}
        };

        // this.router.navigate(['/app/gen/dataTable'],navigationExtras)

        this.router.navigate(["/app/gen/fichedetail", this.idProjet]);
        
    }
}
