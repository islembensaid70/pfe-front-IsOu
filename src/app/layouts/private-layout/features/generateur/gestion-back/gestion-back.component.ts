import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from "@angular/core";

import {
  FormControl,
  FormGroup,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators
} from "@angular/forms";
import { SharedService } from "@shared/services/sharedWs/shared.service";
import { CriteriaSearch, Pagination, RequestObject, SearchObject, Sort } from "@shared/models";
import { ConstanteWs } from "@shared/constantes/ConstanteWs";
import { ResponseObject } from "@shared/models/ResponseObject";
import { ToastService } from "@shared/services";
import { isEmptyValue } from "@shared/tools";
import { MatDialog } from "@angular/material/dialog";
import { DialogComponent } from "@shared/widgets";
import { DialogMSComponent } from "@privateLayout/features/generateur/gestion-back/dialog-ms/dialog-ms.component";
import { CommonModule } from '@angular/common';
import {
  DialogEditMsComponent
} from "@privateLayout/features/generateur/gestion-back/dialog-edit-ms/dialog-edit-ms.component";
import { Router } from "@angular/router";
import { GenerateurService } from "@privateLayout/shared/services/generateur.service";
import { MatAccordion, MatExpansionPanel } from "@angular/material/expansion";
import { MatStepper } from "@angular/material/stepper";

@Component({
  selector: 'app-gestion-back',
  templateUrl: './gestion-back.component.html',
  styleUrls: ['./gestion-back.component.css'],

})
export class GestionBackComponent implements OnInit {
  springImg = "assets/images/gallery/spring.png";
  reportingImg = "assets/images/gallery/reporting.png";
  adminImg = "assets/images/gallery/administration.png";
  notifImg = "assets/images/gallery/notification.png";
  backEndPath: string
  form: UntypedFormGroup;
  formMs: UntypedFormGroup;
  formMsEureka: UntypedFormGroup;
  listMsType: [];
  connexion = false;
  connexionDEV = false;
  connexionPROD = false;
  connexionTEST = false;
  connexionPREPROD = false;
  params: any = [];
  listMsConfig: any = [];
  listProfils: any = [];
  openedMsList: any = [];
  idProfileSelected: any;
  selectedMs: any;
  msCustomIsClicked = false
  listMsTypeToShow: any[] = [];
  customMsOnly: any[];
  msCustomIsClickedArray: any[] = [];
  @ViewChild('checkboxList', { static: true }) checkboxList: ElementRef<HTMLDivElement>;
  msToCreateArray: any[] = [];
  propsToWriteArray: any[] = [];
  dataBasetables: string[];
  eurekaPortToSend: any;
  clickedPoursuiveDev = false ;
  clickedPoursuiveTest = false ;
  clickedPoursuiveProd = false ;
  clickedPoursuivePreProd = false ;
  panelsOpened= false;
  numberOfMsAndProfileNotOpened: number;
  isFirstClick = true;



  constructor(private generateurService: GenerateurService , private formBuilder: UntypedFormBuilder,private router: Router,
              private sharedService: SharedService , private toast: ToastService,public dialog: MatDialog) { }

  ngOnInit(): void {

    this.initForm();
    this.initFormMs()
    this.initFormEureka()
    this.initMsType()

    this.initPofils()

  }
  @ViewChildren(MatExpansionPanel) panels: QueryList<MatExpansionPanel>;


  checkIfAllPanelsOpened() {
    const allOpened = this.panels.toArray().every(panel => panel.expanded);
    this.panelsOpened = allOpened
    console.log('All panels opened:', allOpened);
  }

  hasEmptyValue(): boolean {

    if (this.numberOfMsAndProfileNotOpened!=0){
      return true
    }


    if (!this.panelsOpened) {
      return true;
    }

    if (!this.getFormMsEurekaControl('eurekaPort').value) {
      return true;
    }

    if (!this.formMs) {
      return true; // Return true if the formMs abstract group is not defined
    }

    const controls = this.formMs['controls'];
    if (!controls || Object.keys(controls).length === 0) {
      return true; // Return true if there are no controls in the formMs abstract group
    }

    for (const controlName in controls) {
      if (controls.hasOwnProperty(controlName)) {
        const control = controls[controlName];
        if (!control.value) {
          return true; // Return true if any control has an empty value
        }
      }
    }

    return false; // Return false if all controls have values or if the formMs abstract group is defined with controls
  }






  private initPofils() {
    const request: RequestObject = <RequestObject>{
      uri: "tProfile/",
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response: ResponseObject) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.listProfils = <[]>response.payload;
          const selectedProfils = localStorage.getItem('selectedProfils');
          const selectedProfilsList = JSON.parse(selectedProfils);
          console.log(selectedProfilsList , 'selectedProfilsList')
          this.listProfils = this.listProfils.filter(obj => selectedProfilsList.includes(obj.idProfile));
          if (this.listProfils.length > 0) {
            this.idProfileSelected = this.listProfils[0].idProfile;
          }
          console.log(this.idProfileSelected)


          console.log(this.listProfils , ' liste des profils ');

        } else {
          console.error("Error in listMsType")
          ;
          this.toast.error();
        }
      },
      error: (error) => {
        console.error("Error in listMsType");
        this.toast.error();
      }
    });

  }
  initMsType() {
    const request: RequestObject = <RequestObject>{
      uri: "tNmTypeMs/",
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response: ResponseObject) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.listMsType = <[]>response.payload;

          this.listMsTypeToShow = this.listMsType.filter(msType => msType["labelType"] === 'Eureka' || msType["labelType"] === 'Gateway');
          this.generateUniqueIdCheckbox();
          console.log(this.listMsType);
          console.log(this.listMsTypeToShow);

        } else {
          console.error("Error in listMsType")
          ;
          this.toast.error();
        }
      },
      error: (error) => {
        console.error("Error in listMsType");
        this.toast.error();
      }
    });
  }


  initSearchObject(data) {
    const searchObject = new SearchObject();
    searchObject.dataSearch = [new CriteriaSearch('idType', data, '=')];
    return searchObject;
  }
  getMsConfig(msType){

    const searchObject = new SearchObject();
    searchObject.dataSearch = [new CriteriaSearch('idType', msType.idType, '='),
      new CriteriaSearch('idProfil', this.idProfileSelected, '=')];
    console.log(searchObject)
    const request: RequestObject = <RequestObject>{
      uri: "tMsConf/data",
      method: ConstanteWs._CODE_POST,
      params: {
        body:  {"dataSearch":searchObject.dataSearch}
      }
    };

    this.sharedService.commonWs(request).subscribe({
      next: (response: ResponseObject) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.listMsConfig = <[]>response.payload.data
          console.log(<[]>response.payload);
          this.initFormCustomMs(msType)

        } else {
          console.error("Error in post ms conf ")
          ;
          this.toast.error();
        }
      },
      error: (error) => {
        console.error("Error in post ms conf ");
        this.toast.error();
      }
    });

  }


  getClickedMsId(e,msType) {


    if (e.target.checked) {

      this.msCustomIsClickedArray.push(msType.uniqueId)
      console.log(this.msCustomIsClickedArray, 'this.msCustomIsClickedArray CHECKED')

      // this.selectedMs =msType.idType
      console.log(msType.idType, 'console.log(e.target.id)')
      // this.getMsConfig(msType.idType)

    }


    else {
      console.log(e.target.id)
      console.log('notif is unchecked');

      this.listMsConfig = null
      this.openedMsList = null

      let index = this.msCustomIsClickedArray.indexOf(msType.uniqueId);
      if (index !== -1) {
        this.msCustomIsClickedArray.splice(index, 1);
      }
      console.log(this.msCustomIsClickedArray , 'msCustomIsClickedArray NOT CHEKDD')


    }

  }

  getFormControl(key) {
    return this.form.controls[key] as UntypedFormControl;
  }
  getFormMsControl(key) {
    return this.formMs.controls[key] as UntypedFormControl;
  }
  getFormMsEurekaControl(key) {
    return this.formMsEureka.controls[key] as UntypedFormControl;
  }
  initForm() {
    const nameDB= ''
    const hostDB= ''
    const portDB= ''
    const userName= ''
    const password= ''
    this.form = this.formBuilder.group({
      nameDB: this.formBuilder.control('', Validators.required),
      hostDB: this.formBuilder.control('', Validators.required),
      portDB: this.formBuilder.control('', Validators.required),
      userName: this.formBuilder.control('', Validators.required),
      password: this.formBuilder.control('', Validators.required),
      portGateWay: this.formBuilder.control('', Validators.required),
      portMs: this.formBuilder.control('', Validators.required),

    });

  }

  initFormEureka() {
    this.formMsEureka = this.formBuilder.group({
      eurekaPort: this.formBuilder.control('', Validators.required),

    });
  }
  initFormMs() {
    this.formMs = this.formBuilder.group({});
  }



  initFormCustomMs(msType) {
    console.log(this.listMsConfig);
    let newItems = this.listMsConfig.map(item => ({ ...item, uniqueId: msType.uniqueId }));

    for (const control of this.listMsConfig) {
      console.log(control);
      if (control.cle == 'spring.datasource.username=') {
        const controlValue = this.getFormControl('userName').value.trim();
        const usernameControl = this.formBuilder.control({ value: controlValue, disabled: true }, Validators.required);
        const uniqueIdConf = `${control.idConf}_${msType.uniqueId}`;
        this.formMs.addControl(uniqueIdConf, usernameControl);
      } else if (control.cle == 'spring.datasource.password=') {
        const controlValue = this.getFormControl('password').value.trim();
        const passwordControl = this.formBuilder.control({ value: controlValue, disabled: true }, Validators.required);
        const uniqueIdConf = `${control.idConf}_${msType.uniqueId}`;
        this.formMs.addControl(uniqueIdConf, passwordControl);
      } else if (control.cle == 'spring.datasource.url=') {
        const urlValue = 'jdbc:postgresql://' + this.getFormControl('hostDB').value.trim() + ':' +
          this.getFormControl('portDB').value.trim() + '/' +
          this.getFormControl('nameDB').value.trim();
        const urlControl = this.formBuilder.control({ value: urlValue, disabled: true }, Validators.required);
        const uniqueIdConf = `${control.idConf}_${msType.uniqueId}`;
        this.formMs.addControl(uniqueIdConf, urlControl);
      }
      else {
        const uniqueIdConf = `${control.idConf}_${msType.uniqueId}`;
        const newControl = this.formBuilder.control('', Validators.required);
        this.formMs.addControl(uniqueIdConf, newControl);
      }
    }

    const updated = newItems.filter(item =>
      !this.openedMsList.some(openedItem =>
        openedItem.idConf === item.idConf && openedItem.uniqueId === item.uniqueId
      )
    );

    this.openedMsList = [...this.openedMsList, ...updated];
  }




  // initFormCustomMs(msType) {
  //   console.log(this.listMsConfig)
  //   let newItems = this.listMsConfig.map(item => ({ ...item, uniqueId: msType.uniqueId }));
  //   for (const control of this.listMsConfig) {
  //     console.log(control)
  //     if (control.cle == 'spring.datasource.username=') {
  //       const controlValue = this.getFormControl('userName').value.trim();
  //       const usernameControl = this.formBuilder.control({ value: controlValue, disabled: true }, Validators.required);
  //       this.formMs.addControl(control.idConf, usernameControl);
  //     }
  //
  //     if (control.cle == 'spring.datasource.password=') {
  //       const controlValue = this.getFormControl('password').value.trim();
  //       const passwordControl = this.formBuilder.control({ value: controlValue, disabled: true }, Validators.required);
  //       this.formMs.addControl(control.idConf, passwordControl);
  //     }
  //
  //     if (control.cle == 'spring.datasource.url=') {
  //       const urlValue = 'jdbc:postgresql://' + this.getFormControl('hostDB').value.trim() + ':'
  //         + this.getFormControl('portDB').value.trim() + '/'
  //         + this.getFormControl('nameDB').value.trim();
  //       const urlControl = this.formBuilder.control({ value: urlValue, disabled: true }, Validators.required);
  //       this.formMs.addControl(control.idConf, urlControl);
  //     }
  //
  //     else {
  //     this.formMs.addControl(control.idConf, this.formBuilder.control('', Validators.required));}
  //
  //   }
  //
  //   const updated = newItems.filter(item =>
  //     !this.openedMsList.some(openedItem =>
  //       openedItem.idConf === item.idConf && openedItem.uniqueId === item.uniqueId
  //     )
  //   );
  //
  //   this.openedMsList = [...this.openedMsList, ...updated];
  //
  //
  //   console.log(this.openedMsList, ' this.openedMsList');
  //   console.log(this.formMs, ' this.formMs ');
  //   console.log(this.formMs.value, ' this.formMs.value, ');
  //   console.log(this.formMs.controls, ' this.formMs.controls');
  // }

  generateMs(): Promise<any>{



    return new Promise<void>((resolve, reject) => {
      this.generateFromProfile().then(() => {






        this.backEndPath = localStorage.getItem('backEndPath')
        console.log(this.backEndPath)
        const listMsSentToReverse = []
        const cloningPromises = []; // create an array to store promises for each clone operation

        this.listMsTypeToShow.forEach((item: any) => {
          if (item.labelType === "Gateway") {
            cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelType)
              .toPromise()
              .then(() => {
                return this.generateurService.cloneGateway(this.backEndPath + '/' + item.labelType).toPromise();
              }));
            item.msPath = this.backEndPath + '/' + item.labelType;
            listMsSentToReverse.push(item)
          } else if (item.labelType === "Eureka") {
            cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelType)
              .toPromise()
              .then(() => {
                return this.generateurService.cloneEureka(this.backEndPath + '/' + item.labelType).toPromise();
              }));
            item.msPath = this.backEndPath + '/' + item.labelType;
            listMsSentToReverse.push(item)
          } else if (item.labelType === "CustomMs" && this.isMsCustomClickedInList(item)) {
            cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelToDisplay)
              .toPromise()
              .then(() => {
                return this.generateurService.cloneBlankMs(this.backEndPath + '/' + item.labelToDisplay).toPromise();
              }));
            item.msPath = this.backEndPath + '/' + item.labelToDisplay;
            listMsSentToReverse.push(item)
          } else if (item.labelType != "CustomMs" && this.isMsCustomClickedInList(item)) {
            if (item.labelType === "GED") {
              cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelType)
                .toPromise()
                .then(() => {
                  return this.generateurService.cloneGed(this.backEndPath + '/' + item.labelType).toPromise();
                }));
              item.msPath = this.backEndPath + '/' + item.labelType;
              listMsSentToReverse.push(item)
            } else if (item.labelType === "Reporting") {
              cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelType)
                .toPromise()
                .then(() => {
                  return this.generateurService.cloneReporting(this.backEndPath + '/' + item.labelType).toPromise();
                }));
              item.msPath = this.backEndPath + '/' + item.labelType;
              listMsSentToReverse.push(item)
            } else if (item.labelType === "Administration") {
              cloningPromises.push(this.generateurService.createDirectory(this.backEndPath, item.labelType)
                .toPromise()
                .then(() => {
                  return this.generateurService.cloneAdministration(this.backEndPath + '/' + item.labelType).toPromise();
                }));
              item.msPath = this.backEndPath + '/' + item.labelType;
              listMsSentToReverse.push(item)
            }
          }
        });

        // Wait for all cloning promises to resolve before navigating to the next page
        Promise.all(cloningPromises).then(() => {

          const eurekaPort = this.getFormMsEurekaControl('eurekaPort').value.trim();
          this.eurekaPortToSend = null
          if (eurekaPort != null && eurekaPort !== '') {
            this.eurekaPortToSend = eurekaPort
          }


          localStorage.setItem('listMsSentToReverse', JSON.stringify(listMsSentToReverse));
          console.log(this.propsToWriteArray, 'this.propsToWriteArray')
          this.generateurService.generateProperties(this.propsToWriteArray).subscribe(response => (
            this.generateurService.generateYml(this.propsToWriteArray, listMsSentToReverse, this.eurekaPortToSend).subscribe(res => console.log(res)),
              console.log(response)))
          this.router.navigate(['/app/gen/back-reverse']).then(r => {
            console.log('directories created ');
          });
        }).catch((error) => {
          console.error(error);
        });















        /*************************************/
        resolve();
      }).catch((error) => {
        // Handle any errors that occur during generateFromProfile()
        reject(error);
      });
    });



















  }



  closeMsCustom(){
    this.numberOfMsAndProfileNotOpened += 1;
    console.log('close')
    console.log(this.numberOfMsAndProfileNotOpened)

  }

  openMsCustom(msType){
    this.getMsConfig(msType)
    this.checkIfAllPanelsOpened()
    this.numberOfMsAndProfileNotOpened -= 1;
    console.log(this.numberOfMsAndProfileNotOpened)

  }
  poursuivreClick(profileName) {
    if (this.isFirstClick) {
      // Execute your code here
      console.log("First click executed!");
      this.numberOfMsAndProfileNotOpened = (this.listMsTypeToShow.length-1)*this.listProfils.length
      console.log(this.numberOfMsAndProfileNotOpened)
      // Set the flag to false to prevent further execution
      this.isFirstClick = false;
    }


    if (profileName === 'dev'){
      if (this.connexionDEV){this.clickedPoursuiveDev = true}
        else this.toast.error('Veuillez tester la connectivité à la base de données')
    }

    if (profileName === 'test'){
      if (this.connexionTEST){this.clickedPoursuiveTest = true}

    }
    if (profileName === 'prod'){
      if (this.connexionPROD){this.clickedPoursuiveProd = true}

    }
    if (profileName === 'pre-prod'){
      if (this.connexionPREPROD){this.clickedPoursuivePreProd = true}

    }
  }
  testConnection(profileName) {

    let urlBD =  'jdbc:postgresql://'+this.getFormControl('hostDB').value.trim()+':'
        +this.getFormControl('portDB').value.trim()+'/'
        +this.getFormControl('nameDB').value.trim()
    console.log(urlBD)

    const password = this.getFormControl('password').value.trim()
    const username = this.getFormControl('userName').value.trim()



    this.generateurService.testConnection(urlBD, password, username).subscribe((response) => {
      if (response === 'Connexion avec la base de données OK') {


        if (profileName === 'dev'){
          this.connexionDEV = true
          this.clickedPoursuiveDev =false
        localStorage.setItem('url', urlBD);
        localStorage.setItem('password',password);
        localStorage.setItem('username', username);
        this.getDataBaseSchemas(urlBD ,password, username)
        }

        if (profileName === 'test'){
          this.connexionTEST = true
          this.clickedPoursuiveTest =false
        }
        if (profileName === 'prod'){
          this.connexionPROD = true
          this.clickedPoursuiveProd =false
        }
        if (profileName === 'pre-prod'){
          this.connexionPREPROD = true
          this.clickedPoursuivePreProd =false
        }

        this.toast.success('Connexion OK' )
      } else if (response === "Base de données n'existe pas") {
        if (profileName === 'dev'){
          this.connexionDEV = false
          this.clickedPoursuiveDev =false
        }

        if (profileName === 'test'){
          this.connexionTEST = false
          this.clickedPoursuiveTest =false
        }
        if (profileName === 'prod'){
          this.connexionPROD = false
          this.clickedPoursuiveProd =false
        }
        if (profileName === 'pre-prod'){
          this.connexionPREPROD = false
          this.clickedPoursuivePreProd =false
        }
        this.toast.error('Vérifiez vos coordonnées de la base de donnée')
      }
    });

  }
  getDataBaseSchemas(urlBD ,password, username) {
    this.generateurService.getDataBaseSchemas(urlBD ,password, username).subscribe(tables =>
        localStorage.setItem('tables', JSON.stringify(tables)));
  }


  createMS() : Promise<void> {
    return new Promise((resolve, reject) => {
      const checkboxInputs = this.checkboxList.nativeElement.querySelectorAll('input');
      const checkedMsTypes = [];

      checkboxInputs.forEach(input => {
        if (input.checked) {
          const msType = this.listMsTypeToShow.find(msType => msType.uniqueId === input.id);
          if (msType) {
            checkedMsTypes.push(msType);
          }
        }
      });
      console.log(checkedMsTypes, ' TEKHDEM 3LA HEDHI')

      this.msToCreateArray =  [];


      checkedMsTypes.forEach(msType => {
        let obj = {};
        const idProjetString = localStorage.getItem("idProjet");
        const pathMs = localStorage.getItem("backEndPath");
        const msName = msType.labelToDisplay ? msType.labelToDisplay : msType.labelType
        const idProjet = parseInt(idProjetString);
        obj['idProjet'] = idProjet;
        obj['pathMs'] = pathMs+"/"+msName
        obj['pathGateway'] = "pathGateway";
        obj['nomMs'] = msType.labelToDisplay ? msType.labelToDisplay : msType.labelType;

        console.log(obj);
        // add any additional code you want to execute for each element in this.listMsTypeToShow here
        this.msToCreateArray.push(obj)
      });
      console.log(this.msToCreateArray,'msToCreatejArray');



      const request: RequestObject = <RequestObject>{
        uri: "tMicroservice/saveAll",
        method: ConstanteWs._CODE_POST,
        params: {
          body:  this.msToCreateArray
        }
      };

      this.sharedService.commonWs(request).subscribe({
        next: (response: ResponseObject) => {
          if (response.code == ConstanteWs._CODE_WS_SUCCESS) {


            console.log(<[]>response.payload ,' <[]>response.payload tMicroservice/saveAll');
            //this code to add a idCreatedMs attribute to the list of the microservices after post
            checkedMsTypes.forEach((checkedMsType) => {
              const matchingResponse = <[]>response.payload.find(
                  (res) =>
                      res.nomMs === (checkedMsType.labelToDisplay || checkedMsType.labelType)
              );
              if (matchingResponse) {
                checkedMsType.idCreatedMs = matchingResponse['idMs'];
                checkedMsType.nomMsCreated = matchingResponse['nomMs'];
              }
            });
            this.listMsTypeToShow = checkedMsTypes
            console.log(this.listMsTypeToShow);
            resolve();




          } else {
            console.error("Error in post microservice ")
            ;
            // localStorage.setItem('isMsCreated', 'false');
            this.toast.error();
          }
        },
        error: (error) => {
          console.error("Error in post miscroservice");
          // localStorage.setItem('isMsCreated', 'false');
          this.toast.error();
          reject(error);
        }
      });
    });
  }
  async generateFromProfile() {

    try {
      await this.createMS(); // wait for createMS() to finish before proceeding

      // rest of the code here
    } catch (error) {
      console.error("Error in createMS()", error);
      // handle error here
    }


    ///////////////////////////////////////////////////////////////////////////
    let msValuesArray = [];

    console.log(this.openedMsList, ' this.openedMsList ')
    console.log(this.listMsTypeToShow, ' this.listMsTypeToShow ')
    for (const control of this.openedMsList) {
      // console.log(this.formMs.controls[control.cle].value.trim())
      // console.log(this.formMs.controls[control.cle])
      let obj = {};


      // obj['valeur'] = this.formMs.controls[control.idConf].value.trim()

      obj['idMsConfig'] = control.idConf
      const msType = this.listMsTypeToShow.find(type => type.idType === control.idType && type.uniqueId===control.uniqueId);
      if (msType) {
        if (msType.uniqueId === control.uniqueId) {
          obj['valeur'] = this.formMs.controls[`${control.idConf}_${msType.uniqueId}`].value.trim();
          obj['uni'] = msType.uniqueId
        }

        console.log(msType, 'created ms found')
        obj['idMs'] = msType.idCreatedMs;
      }

      msValuesArray.push(obj)
      // zid if(formMs valid) fel fou9 ... control.id walla na3rach
    }




    console.log(msValuesArray)

    const request: RequestObject = <RequestObject>{
      uri: "tMsConfValue/saveAll",
      method: ConstanteWs._CODE_POST,
      params: {
        body:  msValuesArray
      }
    };

    this.sharedService.commonWs(request).subscribe({
      next: (response: ResponseObject) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {


          console.log(<[]>response.payload);

          const result = [];

          <[]>response.payload.forEach((payloadObj) => {
            const matchingOpenedMsObj = this.openedMsList.filter((openedMsObj) => {
              console.log(openedMsObj,'WIIIIIIIIIIIIIIIIIIIIIIIIISSSSSSSS')
              console.log(payloadObj,'HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
              return payloadObj.idMsConfig === openedMsObj.idConf && payloadObj.uni === openedMsObj.uniqueId;
            })[0];

            if (matchingOpenedMsObj) {
              result.push({
                valeur: payloadObj.valeur,
                idMs: payloadObj.idMs,
                cle: matchingOpenedMsObj.cle,
                idProfil: matchingOpenedMsObj.idProfil,
              });
            }
          });
          result.forEach((obj) => {
            const profile = this.listProfils.find((p) => p.idProfile === obj.idProfil);
            obj.labelProfile = profile.labelProfile;
          });
          result.forEach(resultElem => {
            // console.log('resultresultresultresult', result )
            // console.log('resultElemresultElemresultElemresultElem', resultElem )
            // console.log('this.listMsTypeToShowthis.listMsTypeToShowthis.listMsTypeToShowthis.listMsTypeToShow', this.listMsTypeToShow )
            const listMsTypeToShowElem = this.listMsTypeToShow.find(ms => ms.idCreatedMs === resultElem.idMs);
            console.log('listMsTypeToShowElem', listMsTypeToShowElem )

            if (listMsTypeToShowElem) {
              this.backEndPath=localStorage.getItem('backEndPath')
              resultElem['msPath'] = this.backEndPath+'/'+listMsTypeToShowElem.nomMsCreated;
            }
          });

          this.propsToWriteArray = result
          console.log(result ,'result')
          console.log(this.listMsTypeToShow ,'this.listMsTypeToShow')





        } else {
          console.error("Error in post ms conf value ")
          ;
          this.toast.error();
        }
      },
      error: (error) => {
        console.error("Error in post ms conf value");
        this.toast.error();
      }
    });


  }


  onProfileLabelClick(idProfile) {
    this.idProfileSelected = idProfile

    //this.formMs.reset();
    // if (this.selectedMs != null) {
    //   this.getMsConfig(this.selectedMs)
    // }
    console.log(idProfile , 'idProfileidProfileidProfileidProfileidProfile')
    console.log(this.idProfileSelected)
  }

  changeClickMsState(msType) {
    if(msType.labelType === 'CustomMs'){
      this.msCustomIsClickedArray.push(msType.uniqueId)
      console.log(this.msCustomIsClickedArray, 'this.msCustomIsClickedArray')
    }
  }


  openDialog() {
    const dialogRef = this.dialog.open(DialogMSComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });

    dialogRef.backdropClick().subscribe(() => {
      console.log('Clicked outside the dialog');
    });
  }


  openEureka() {

  }

  closeEureka() {

  }

  openGateway() {

  }

  closeGateway() {

  }

  addToListMsType() {

    const listMsTypeToSend = this.listMsType
        .filter(msType => msType["labelType"] != 'Eureka' && msType["labelType"] != 'Gateway');
    this.sharedService.openDialog(DialogMSComponent
        , { listMsType : listMsTypeToSend }).subscribe((response) => {
      if (response) {
        console.log(response, 'hedhi reponse')
        response.forEach((item) => {
          const foundItem = this.listMsTypeToShow.find((x) => x.idType === item.idType && x.labelToDisplay === item.labelToDisplay);
          if (!foundItem) {
            this.listMsTypeToShow.push(item);
            this.generateUniqueIdCheckbox();
            this.customMsOnly = this.listMsTypeToShow
            this.msCustomIsClickedArray.push(item.uniqueId)
            console.log(item.uniqueId, 'item.uniqueId')
            console.log(this.listMsTypeToShow, 'list to show')
            console.log(this.customMsOnly, 'customMsOnly')
          }
        });

      }
    });

  }


  generateUniqueIdCheckbox() {
    for (let i = 0; i < this.listMsTypeToShow.length; i++) {
      this.listMsTypeToShow[i].uniqueId = `checkbox_${i}`;
    }
  }

  isMsCustomClickedInList(msType): boolean {
    return this.msCustomIsClickedArray.includes(msType.uniqueId);
  }

  openPreDefinedMS() {

  }

  closePreDefinedMS() {

  }


  test() {

    console.log(this.listMsTypeToShow)
    console.log(this.openedMsList)
    console.log(this.formMs.controls)
  }


  removeFromListToShow(msType) {
    console.log(msType)
    const index = this.listMsTypeToShow.findIndex(obj => obj.uniqueId === msType.uniqueId);
    if (index >= 0) {
      this.listMsTypeToShow.splice(index, 1);
    }
    let indexCheck = this.msCustomIsClickedArray.indexOf(msType.uniqueId);
    if (indexCheck !== -1) {
      this.msCustomIsClickedArray.splice(indexCheck, 1);
    }
    ////delete mel checkbox checked list
    console.log(this.listMsTypeToShow)
    console.log(this.msCustomIsClickedArray)
  }


  onClickEditLabel(msType) {
    const dialogRef = this.dialog.open(DialogEditMsComponent, {

      data: { label: msType.labelToDisplay , uniqueid : msType.uniqueId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const updatedList = this.listMsTypeToShow.map(item => {
          if (item.uniqueId === result.uniqueid) {
            return {
              ...item,
              labelToDisplay: result.label
            };
          }
          this.msCustomIsClickedArray.push(item.uniqueId)
          // let indexCheck = this.msCustomIsClickedArray.indexOf(msType.uniqueId);
          // if (indexCheck !== -1) {
          //   this.msCustomIsClickedArray.splice(indexCheck, 1);
          // }
          return item;
        });
        this.listMsTypeToShow = updatedList

        console.log(updatedList);
      }
    });
  }


  isOnFirstStep(stepper: MatStepper): boolean {
    return stepper.selectedIndex === 0;
  }
}
