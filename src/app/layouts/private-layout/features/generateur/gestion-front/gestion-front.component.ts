import { Component, Inject, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { RequestObject, SelectMetadata } from "@shared/models";
import { ConstanteWs } from "@shared/constantes/ConstanteWs";
import { ResponseObject } from "@shared/models/ResponseObject";
import { ToastService } from "@shared/services";
import { SharedService } from "@shared/services/sharedWs/shared.service";
import { FormBuilder, FormControl, FormGroup, UntypedFormControl, Validators } from "@angular/forms";
import { ProjetComponent } from "@privateLayout/features/generateur/projet/projet.component";
import { GenerateurService } from "@privateLayout/shared/services/generateur.service";
import { Router } from "@angular/router";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { selectedLangues } from "@privateLayout/shared/constantes/generateur/metadata";

@Component({
  selector: "app-gestion-front",
  templateUrl: "./gestion-front.component.html",
  styleUrls: ["./gestion-front.component.css"]
})
export class GestionFrontComponent implements OnInit {
  listLang: any;
  selectedLang = [];
  langPrj = [];
  id: number;
  selectedLangues = selectedLangues;
  form: FormGroup;
  lang:UntypedFormControl

  ngOnInit(): void {

  }

  constructor(private sharedService: SharedService,
              private toast: ToastService,
              private _formBuilder: FormBuilder,
              private router: Router,
              private dialogRef: MatDialogRef<GestionFrontComponent, { data: any }>,
              @Inject(MAT_DIALOG_DATA) data
  ) {
    console.log("dataa", data);
    this.id = data.idProjet;
    this.listLang = data.listLang;
    this.initFormLg();

  }

  /**Recuperer les langues selectionnes*/
  getSelectedLang() {
    console.log("fef", this.selectedLang);
    this.selectedLang = this.lang.value;
    this.sendPrjLangues();
    console.log(this.selectedLang);

  }

  /**Init form*/
  initFormLg() {
    // this.form = this._formBuilder.group({
      this.lang= this._formBuilder.control(null, Validators.required)



  }

  getFormControl(key) {
    return this.form.get(key) as UntypedFormControl;
  }

  /*Post des langues relatives au projet */
  sendPrjLangues() {
    console.log("id from gestion front");
    console.log(this.id);

    for (let i = 0; i < this.selectedLang.length; i++) {
      const body = {};
      body["idPrj"] = this.id;

      body["idLang"] = this.selectedLang[i];
      this.langPrj.push(body);


    }
    console.log("array");

    console.log(this.langPrj);
    const request: RequestObject = <RequestObject>{

      uri: "tProjetLangue/lgp/",
      method: ConstanteWs._CODE_POST,
      params: {
        body: this.langPrj
      }
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response: ResponseObject) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          console.log("payload lang projet ");
          console.log(<[]>response.payload);
          this.toast.success("Langues ajoutées au projet avec sucées");
          this.toast.success("Fichiers i18n crées");

          this.router.navigate(["/app/gen/details", this.id]);

        } else {
          console.error("Error in post lang projet")
          ;
          this.toast.error();
        }
      },
      error: (error) => {
        console.error("Error in post lang projet");
        this.toast.error();
      }
    });
  }


}


