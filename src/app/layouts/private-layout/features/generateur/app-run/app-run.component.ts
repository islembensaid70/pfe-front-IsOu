import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-run',
  templateUrl: './app-run.component.html',
  styleUrls: ['./app-run.component.css']
})
export class AppRunComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  async openStackBlitz() {
    const projectId = 'my-project-id';
    const absolutePath = '/src/app/my-component.ts';
    const options = {
      view: 'editor',
      hideExplorer: true,
      hideNavigation: true,
      hideDevTools: true,
      forceRefresh: true,
      absolutePath,
    };
  }

}
