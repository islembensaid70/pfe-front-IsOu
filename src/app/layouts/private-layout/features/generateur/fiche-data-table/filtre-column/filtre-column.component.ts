import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {FormArray, FormBuilder, FormGroup, UntypedFormArray, UntypedFormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {
    entityMetadata, filtreTypesMetadata,
    iconsMetadata,
    selectedLangue,
    typeCol,filtreSpeMetadata
} from "@privateLayout/shared/constantes/generateur/metadata";
import {COMMON_Filter_TYPES, COMMON_TYPES_CODES,specifList} from "@shared/constantes/Constante";
import {SelectMetadata} from "@shared/models";
@Component({
    selector: 'app-filtre-column',
    templateUrl: './filtre-column.component.html',
    styleUrls: ['./filtre-column.component.css']
})
export class FiltreColumnComponent implements OnInit {
    formColumn: FormGroup
    entityMetadata = entityMetadata;
    selectedEntity: String;
    attributes: any;
    data: any;
    selectLangue: SelectMetadata;
    listTypesFiltre = Object.keys(COMMON_Filter_TYPES).map(key => ({key, value: COMMON_Filter_TYPES[key]}));
    filtreTypesMetadata = filtreTypesMetadata
    fields:FormArray
    filtreSpeMetadata = filtreSpeMetadata
    specifList = specifList

    constructor(private sharedService: SharedService,
                private toast: ToastService,
                private _formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<FiltreColumnComponent, { data: any }>,
                @Inject(MAT_DIALOG_DATA) data) {
        this.data = data
        if (data.formField == null) {
            this.initFilterColumn()

        } else {
            this.setValueForm()
        }

        this.selectedEntity = data.selectedEntity;
        this.attributes = data.attributes;
        console.log("data form", data)
        this.selectLangue = selectedLangue;

    }

    ngOnInit(): void {

    }

    initFilterColumn() {
        const itemControls = [];
        for (let i = 0; i < this.data['listLangues'].length; i++) {
            let formGroup = this._formBuilder.group({
                labelg: this._formBuilder.control(this.data["listLangues"][i]["labelLang"]),
                keylg: this._formBuilder.control(this.data["listLangues"][i]["keyLang"]),

                content: this._formBuilder.control(null, [Validators.required])
            });
            itemControls.push(formGroup);
        }


        this.formColumn =
            this._formBuilder.group({
                    unilang: this._formBuilder.control(true),
                    def: this._formBuilder.array([
                        this._formBuilder.group({

                            lg: this._formBuilder.control(null, [Validators.required]),
                            content: this._formBuilder.control(null, [Validators.required])
                        })]),
                    labels: this._formBuilder.array(itemControls),


                    type: this._formBuilder.control(COMMON_TYPES_CODES["TEXT"], [Validators.required]),
                    isSortable: this._formBuilder.control(true),
                    isCombined: this._formBuilder.control(false),
                    btns: this._formBuilder.array([
                        this._formBuilder.control(null)


                    ])
                }
            );


    }

    setValueForm() {
        console.log("dfr", this.data.formField['def'].value)
        this.formColumn =
            this._formBuilder.group({
                    unilang: this._formBuilder.control(true),
                    def: this._formBuilder.array([this.data.formField['def'].value]),
                    type: this._formBuilder.control(this.data.formField['type']),
                    specificSearch: this._formBuilder.control(this.data.formField['specificSearch']),
                    key: this._formBuilder.control(this.data.formField['key']),


                }
            );


    }

    AddtoFilter() {
       // this.data['fields'].removeAt(this.data['index'])
        this.data['fields'].push(this.formColumn)
        this.onPrecedent(this.formColumn)

    }
    onPrecedent(form) {
        console.log("formm on click",form)
        this.dialogRef.close({data:form});
    }

    getcontrol(formArray, k, key) {
        return formArray.controls[k].get(key) as UntypedFormControl;
    }

    getFormControl(key) {
        return this.formColumn.get(key) as UntypedFormControl;
    }

    addLangCol() {

        const langControl = this._formBuilder.group({
            lg: this._formBuilder.control(null),
            content: this._formBuilder.control(null)


        });

        (<FormArray>this.formColumn.get("def")).push(langControl);


    }

    get def() {
        return this.formColumn.get("def") as UntypedFormArray;
    }

    get labels() {
        return this.formColumn.get("labels") as UntypedFormArray;

    }
}
