import {Component, Inject, OnInit} from "@angular/core";
import {
    AbstractControl,
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    UntypedFormArray,
    UntypedFormControl,
    Validators, ɵElement, ɵFormGroupRawValue, ɵGetProperty, ɵTypedOrUntyped
} from "@angular/forms";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RequestObject, SearchObject, SelectMetadata} from "@shared/models";
import {COMMON_TYPES_CODES} from "@shared/constantes/Constante";
import {
    entityMetadata,
    iconsMetadata,
    selectedLangue,
    typeCol
} from "@privateLayout/shared/constantes/generateur/metadata";
import {Icons} from "@shared/constantes/Icons";
import {concat} from "rxjs";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";

@Component({
    selector: "app-column-detail",
    templateUrl: "./column-detail.component.html",
    styleUrls: ["./column-detail.component.scss"]
})
export class ColumnDetailComponent implements OnInit {
    formDaTab: FormGroup;
    formColumn: FormGroup;
    listLang: any;
    searchObject: SearchObject;
    params = {};
    data: any;
    selectLangue: SelectMetadata;
    selectedEntity: String;
    attributes: any;
    entityMetadata = entityMetadata;
    iconsMetadata = iconsMetadata;
    typeCol = typeCol;
    selectedLang = [];
    selectedContent = [];
    COMMON_TYPES_CODESS = Object.keys(COMMON_TYPES_CODES).map(key => ({key, value: COMMON_TYPES_CODES[key]}));
    icons = Object.keys(Icons).map(keyIcon => ({keyIcon, valueIcon: Icons[keyIcon]}));

    responsePayloadCol: any = {};
    formFilter: FormGroup

    constructor(private sharedService: SharedService,
                private toast: ToastService,
                private _formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<ColumnDetailComponent, { data: any }>,
                @Inject(MAT_DIALOG_DATA) data) {

        this.formDaTab = data.formDaTab;
        this.params = data.params;
        this.listLang = data.responsePayload;
        this.selectedEntity = data.selectedEntity;
        this.attributes = data.attributes;

        this.selectLangue = selectedLangue;
        this.responsePayloadCol = data.responsePayloadCol;
        this.formFilter = data.formFilter
        this.data = data;
        this.icons.forEach(icon => {
            icon.valueIcon["keyIcon"] = icon.keyIcon;
        });

    }

    ngOnInit(): void {

        this.initFormCol();


    }


    onPrecedent(form) {
        this.dialogRef.close({data: form});
    }

    getFormControl(key) {
        return this.formColumn.get(key) as UntypedFormControl;
    }

    get def() {
        return this.formColumn.get("def") as UntypedFormArray;
    }

    get labels() {
        return this.formColumn.get("labels") as UntypedFormArray;

    }

    get btns() {
        return this.formColumn.get("btns") as UntypedFormArray;

    }


    initFormCol() {
        const itemControls = [];
        for (let i = 0; i < this.data['listLangues'].length; i++) {
            let formGroup = this._formBuilder.group({
                labelg: this._formBuilder.control(this.data["listLangues"][i]["labelLang"]),
                keylg: this._formBuilder.control(this.data["listLangues"][i]["keyLang"]),

                content: this._formBuilder.control(null, [Validators.required])
            });
            itemControls.push(formGroup);
        }
        this.formColumn =
            this._formBuilder.group({
                    unilang: this._formBuilder.control(true),
                    inFilter: this._formBuilder.control(true),
                    def: this._formBuilder.array([
                        this._formBuilder.group({

                            lg: this._formBuilder.control(null, [Validators.required]),
                            content: this._formBuilder.control(null, [Validators.required])
                        })]),
                    labels: this._formBuilder.array(itemControls),


                    type: this._formBuilder.control(COMMON_TYPES_CODES.TEXT, [Validators.required]),
                    isSortable: this._formBuilder.control(true),
                    isCombined: this._formBuilder.control(false),
                    btns: this._formBuilder.array([
                        this._formBuilder.control(null)


                    ])
                }
            );


    }


    ajouterCol() {
        (<FormArray>this.formDaTab.get("columns")).push(this.formColumn);
        if (this.formColumn.get('inFilter').value==true) {
            this.onPrecedent(this.formColumn);

        }
        else
            this.onPrecedent(null)

    }

    get columns() {
        return this.formDaTab.get("columns") as FormArray;

    }

    getcontrol(formArray, k, key) {
        return formArray.controls[k].get(key) as UntypedFormControl;
    }

    addLangCol() {

        const langControl = this._formBuilder.group({
            lg: this._formBuilder.control(null),
            content: this._formBuilder.control(null)


        });
        this.setLgCont(langControl.get("lg"), langControl.get("content"));

        (<FormArray>this.formColumn.get("def")).push(langControl);

    }

    addBtnCol() {
        const keyIcon = this._formBuilder.control(null);
        this.btns.push(keyIcon);


    }

    delLangCol(k: number) {

        this.def.removeAt(k);
        // this.setLgCont();

    }


    setLgCont(lgControl: AbstractControl<ɵGetProperty<ɵTypedOrUntyped<{
        [K in keyof {
            lg: FormControl<unknown | null>;
            content: FormControl<unknown | null>
        }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null>
    }, ɵFormGroupRawValue<{
        [K in keyof {
            lg: FormControl<unknown | null>;
            content: FormControl<unknown | null>
        }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null>
    }>, any>, "lg">>, contentControl: AbstractControl<ɵGetProperty<ɵTypedOrUntyped<{
        [K in keyof {
            lg: FormControl<unknown | null>;
            content: FormControl<unknown | null>
        }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null>
    }, ɵFormGroupRawValue<{
        [K in keyof {
            lg: FormControl<unknown | null>;
            content: FormControl<unknown | null>
        }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null>
    }>, any>, "content">>) {
        this.def.controls.forEach((control) => {
            this.selectedLang.push(control.getRawValue().lg);
            this.selectedContent.push(control.getRawValue().content);
        });


        let lg = lgControl.value;

        // console.log("selectedContent", this.selectedContent);
        this.selectedContent.forEach((selectedValue: string) => {
            this.attributes = this.attributes.filter((option: any) => option.att !== selectedValue);
        });
        lgControl.patchValue(lg);


        let cont = contentControl.value;
        this.selectedLang.forEach((selectedValue: string) => {
            this.data["listLangues"] = this.data["listLangues"].filter((option: any) => option.keyLang !== selectedValue);
        });
        contentControl.patchValue(cont);


    }


    getcontrolArr(formArray: FormArray<any>, i: number) {

        return formArray.controls[i] as UntypedFormControl;
    }


}
