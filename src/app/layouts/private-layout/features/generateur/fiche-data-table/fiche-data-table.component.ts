import {Component, OnChanges, OnInit, ViewChild} from "@angular/core";
import {
    AbstractControl,
    FormArray,
    FormGroup,
    UntypedFormBuilder,
    UntypedFormControl,
    Validators
} from "@angular/forms";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {
    ColumnDetailComponent
} from "@privateLayout/features/generateur/fiche-data-table/column-detail/column-detail.component";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {Pagination, RequestObject, SearchObject, SelectMetadata} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {FicheDaTable} from "@privateLayout/shared/models/FicheDaTable";
import {
    controllerDetail, controllerMappingDetail, filtreSpeMetadata, filtreTypesMetadata,
    selectedMs,
    selectedOrient,
    selectedTab, typeCol,
} from "@privateLayout/shared/constantes/generateur/metadata";
import {onAction} from "@shared/tools";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";
import {COMMON_Filter_TYPES, COMMON_TYPES_CODES, specifList} from "@shared/constantes/Constante";
import {Subscription} from "rxjs";
import {Icons} from "@shared/constantes/Icons";
import {FilterComponent} from "@shared/widgets/filter/filter.component";
import {
    FiltreColumnComponent
} from "@privateLayout/features/generateur/fiche-data-table/filtre-column/filtre-column.component";


@Component({
    selector: "app-fiche-data-table",
    templateUrl: "./fiche-data-table.component.html",
    styleUrls: ["./fiche-data-table.component.css"]
})
export class FicheDataTableComponent implements OnInit {
    formDaTab: FormGroup;
    formFilter: FormGroup
    compInfo: FormGroup;
    panelOpenState: boolean;
    listOrien = [{ex: "landscape"}, {ex: "portrait"}];
    params = {};
    responsePayload: any = {};
    idProjet;
    listMs: any;
    selectedMs = selectedMs;
    formInfo: FormGroup;
    entities: any;
    selectedTab = selectedTab;
    selectedOrient = selectedOrient;
    selectedEntity: String;
    node: any;
    responsePayloadCol: any = {code: 200, payload: []};
    searchObject: SearchObject;
    filtreTypesMetadata = filtreTypesMetadata
    attributesEnt: any;
    attributes: any;
    mapConstWs: any;
    cleMs: any;
    displayedColumns = [];
    displayedColumnsFiltre = [];
    listTypesFiltre = Object.keys(COMMON_Filter_TYPES).map(key => ({key, value: COMMON_Filter_TYPES[key]}));
    mapping;

    responseColFiltre: any = {code: 200, payload: []};


    constructor(private formBuilder: UntypedFormBuilder,
                private generateurService: GenerateurService,
                private sharedService: SharedService, private toast: ToastService,
                private router: Router, private activatedRoute: ActivatedRoute) {


        this.idProjet = this.activatedRoute.snapshot.params.idProjet;
        this.initMs();
        this.displayedColumns = ["key", "lg", "type", "delete"];
        this.displayedColumnsFiltre = ["key", "type", "specificSearch", "update"]
        // this.displayedColumnsFiltre = ["key", "lg", "type", "delete", "edit"]

    }

    ngOnInit(): void {
        this.searchObject = this.initSearchObject();

        this.node = this.generateurService.node;
        console.log("node", this.node);
        this.initFormComp();
        this.initformDaTab();
        this.initFormInfo();
        this.generateurService.formDataTable = this.formDaTab;
        this.getconstantesWs();
        this.initFormFilter()


    }


    initSearchObject() {
        const searchObject = new SearchObject();
        searchObject.pagination = new Pagination(0, 10);
        return searchObject;
    }


    initFormInfo() {
        this.formInfo = this.formBuilder.group({
            microservice: this.formBuilder.control(null, Validators.required),
            entite: this.formBuilder.control(null, Validators.required),
            controlleur: this.formBuilder.control(null, Validators.required),
            mapping: this.formBuilder.control(null, Validators.required)

        });

    }

    initFormComp() {
        this.compInfo = this.formBuilder.group({
            nomComp: this.formBuilder.control("", [Validators.required, Validators.pattern(/^[a-zA-Z\-]+$/)
            ]),
            pathComp: this.formBuilder.control("", Validators.required)
        });

    }


    getFormControl(form, key) {
        return form.get(key) as UntypedFormControl;
    }

    async initlangue() {

        const request: RequestObject = <RequestObject>{
            uri: "tProjet/getProjLang/",
            params: {
                path: [this.idProjet]

            },
            method: ConstanteWs._CODE_POST
        };
        return new Promise<void>((resolve, reject) => {
            this.sharedService.commonWs(request).subscribe({
                next: (response) => {
                    if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                        this.responsePayload = response.payload;
                        resolve();


                    } else {
                        console.error(
                            `Error in /initLangueProjet, error code :: ${response.code}`
                        );
                        this.toast.error();
                        reject();
                    }
                },
                error: (error) => {
                    console.error(
                        `Error in ColumnComponent/initLangueProjet, error :: ${error}`
                    );
                    this.toast.error();
                    reject();

                }
            });


        });

    }

    initMs() {
        const request: RequestObject = <RequestObject>{
            uri: "tMicroservice/prj/",
            params: {
                path: [this.idProjet]
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.listMs = response.payload;


                } else {
                    console.error(
                        `Error in FicheDataTableComponent/initMs, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in FicheDataTableComponent/initMs, error :: ${error}`
                );
                this.toast.error();
            }
        });
    }

    initTables(selectedMs) {
        this.selectedTab.disabled = false;
        const request: RequestObject = <RequestObject>{
            uri: "tProjet/InfoClass",
            params: {
                query:
                    {
                        directoryBack: selectedMs.pathMs.replaceAll("\\", "/")
                    }
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.entities = response.payload;


                } else {
                    console.error(
                        `Error in FicheDataTableComponent/initTables, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in FicheDataTableComponent/initTables, error :: ${error}`
                );
                this.toast.error();
            }
        });
    }


    initEntCont(selectedMs) {
        this.initTables(selectedMs);
        //  this.initControllers(selectedMs);
        this.getkeymsfromfile(selectedMs);
    }

    initformDaTab() {
        this.formDaTab = this.formBuilder.group({
            title: this.formBuilder.control(""),
            hasAdd: this.formBuilder.control(true, Validators.required),
            hasPagination: this.formBuilder.control(true, Validators.required),
            hasFilter: this.formBuilder.control(true, Validators.required),
            hasExport: this.formBuilder.control(true, Validators.required),
            exportOrientation: this.formBuilder.control("portrait", Validators.required),
            columns: this.formBuilder.array([])
        });


    }


    initFormFilter() {
        this.formFilter = this.formBuilder.group({
            title: this.formBuilder.control(""),
            fields: this.formBuilder.array([])


        })
    }

    async setAttributes() {

        let classPath = this.selectedEntity["classPath"];
        let className = (this.selectedEntity)["className"];
        let classPackage = this.selectedEntity["classPackage"];

        const request: RequestObject = <RequestObject>{
            uri: "tProjet/getClassAttributes",
            params: {
                body: {
                    classPath: classPath,
                    className: className,
                    classPackage: classPackage,

                }
            },
            method: ConstanteWs._CODE_POST

        };
        return new Promise<void>((resolve, reject) => {

            this.sharedService.commonWs(request).subscribe({
                next: (response) => {
                    if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                        this.attributesEnt = response.payload;


                        this.attributes = [];
                        for (let i = 0; i < this.attributesEnt.length; i++) {
                            const att = this.attributesEnt[i];
                            this.attributes.push({att});

                        }
                        this.attributes.push({att: "Action"});
                        resolve();

                    } else {
                        console.error(
                            `Error in FicheDataTableComponent/initMappingsCont, error code :: ${response.code}`
                        );
                        this.toast.error();
                        reject();

                    }
                },
                error: (error) => {
                    console.error(
                        `Error in FicheDataTableComponent/initControllers, error :: ${error}`
                    );
                    this.toast.error();
                    reject();

                }
            });


        });


    }

    async openColumnDialog() {
        {

            await this.initlangue();
            await this.setAttributes();

            this.sharedService.openDialog(ColumnDetailComponent
                , {
                    formDaTab: this.formDaTab,
                    params: this.params,
                    listLangues: this.responsePayload,
                    entites: this.entities,
                    selectedEntity: this.selectedEntity,
                    responsePayloadCol: this.responsePayloadCol,
                    attributes: this.attributes,
                    formFilter: this.formFilter
                }).subscribe(async (response) => {
                if (response != null) {
                    if (response.get('inFilter').value === true) {
                        console.log("response",response.get('type').value)
                        if (response.get('type').value == COMMON_Filter_TYPES.Text) {
                            response.addControl('specificSearch', this.formBuilder.control('upper_like'))
                            this.fields.push(response)

                        }
                        if (response.get('type').value == COMMON_Filter_TYPES.MONTANT) {
                            response.addControl('specificSearch', this.formBuilder.control('='))
                            this.fields.push(response)

                        }
                        if (response.get('type').value == COMMON_Filter_TYPES.DATE) {

                            response.addControl('specificSearch', this.formBuilder.control('>= et <='))
                            this.fields.push(response)




                        }

                    }

                }
                this.setColumnsValues();
                //this.setFiltreColumns()


            });
        }

    }

    async openFilterColumn(element, index) {
        await this.initlangue();
        await this.setAttributes();
        this.sharedService.openDialog(FiltreColumnComponent, {
            attributes: this.attributes,
            formFilter: this.formFilter,
            listLangues: this.responsePayload,
            index: index,
            fields: this.fields,
            formField: element
        }).subscribe(async (response) => {
            if (response != null) {
                if (response) {
                    console.log("response", response)
                    this.setFiltreColumns();

                    // this.fields.push(response)
                }

            }

        });

    }


    getconstantesWs() {
        const request: RequestObject = <RequestObject>{
            uri: "tProjet/getMapConstanteMS/",
            params: {
                path: [this.idProjet]

            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.mapConstWs = response.payload;


                } else {
                    console.error(
                        `Error in /getconstantesWs, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ColumnComponent/getconstantesWs, error :: ${error}`
                );
                this.toast.error();

            }
        });


    }


    getkeymsfromfile(selectedms) {

        const result = Object.entries(this.mapConstWs).find(([key, val]) => val === selectedms.nomMs);
        this.cleMs = result[0];

        return result[0];


    }


    getForm() {
        const val = this.formInfo.get("entite").value["className"];
        const valRef = "Table" + val;
        const valRefFil = "Filtre" + val
        const myControl = this.formBuilder.control(valRef);
        const myControlFil = this.formBuilder.control(valRefFil);

        this.formDaTab.addControl("ref", myControl);
        this.formFilter.addControl("ref", myControlFil);
        console.log("cles", this.cleMs);
        let nom = val.substring(1)
        this.mapping = "t" + nom + "/data"
        this.formDaTab.addControl("uri", this.formBuilder.control(this.mapping));
        this.formDaTab.addControl("microservice", this.formBuilder.control(this.cleMs));
        console.log("rgeg", this.formInfo.get("microservice").value);

        const tcomposantt = {
            idProjet: this.idProjet,
            nomComp: this.compInfo.get("nomComp").value,
            routeComp: this.compInfo.get("pathComp").value
        };
        console.log("form", this.formDaTab.value);
        const request: RequestObject = <RequestObject><unknown>{
            uri: "tComposant/",
            params: {

                body: {
                    fileNode: this.node,
                    tcomposant: tcomposantt,
                    formData: this.formDaTab.getRawValue(),
                    filtreForm: this.formFilter.getRawValue()

                }

            },
            method: ConstanteWs._CODE_POST
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.responsePayload = response.payload;
                    console.log(this.responsePayload);
                    this.toast.success("Composant Data Table  " + this.compInfo.get("nomComp").value + "  géneré avec succés");
                    this.formDaTab.reset();
                    this.responsePayloadCol = []
                    this.formInfo.reset();
                    this.compInfo.reset();
                    this.router.navigate(['app/gen/details', this.idProjet])

                } else {
                    console.error(
                        `Error in ComposantComponent/ComposantModule, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/postModule, error :: ${error}`
                );
                this.toast.error();
            }
        });
        //this.setFilterFields()
        console.log("datatable form",this.formDaTab.value)
        console.log("formm filter", this.formFilter.value)

    }

    onAdd() {
        this.selectedEntity = this.formInfo.get("entite").value;
        this.openColumnDialog();

    }


    setColumnsValues() {
        this.responsePayloadCol.payload = []
        this.responseColFiltre.payload = []

        let columns = this.formDaTab.get("columns") as FormArray;
        let fields = this.formFilter.get("fields") as FormArray;

        for (let i = 0; i < columns.length; i++) {
            let Col = this.setColumn(columns.controls[i])[0]

            this.responsePayloadCol.payload.push(Col)

        }
        for (let i = 0; i < fields.length; i++) {
            let Col1 = this.setColumn(fields.controls[i])[1]

            this.responseColFiltre.payload.push(Col1)

        }


    }


    setColumn(column: AbstractControl) {
        let def = column.get("def") as FormArray;
        let labels = column.get("labels") as FormArray;

        let key = "";
        let Col = {
            key: "",
            type: "",
            isSortable: "",
            lg: "",
        };
        let ColF = {
            key: "",
            type: "",
            lg: "",
            specificSearch: "",
            def: new FormArray([]),
            labels: new FormArray([])

        };


        if (def.length == 1) {
            key = def.controls[0].get("content").value;
            Col.key = key;
            Col.type = column.get("type").value;
            Col.isSortable = column.get("isSortable").value;
            Col.lg = "Unilingue";
            ColF.key = key;
            ColF.type = column.get("type").value;
            ColF.def = def
            ColF.labels = labels
            Col.lg = "Unilingue";
            if(ColF.type==COMMON_Filter_TYPES.Text){
                ColF.specificSearch='upper_like'
            }
            if(ColF.type==COMMON_Filter_TYPES.MONTANT){
                ColF.specificSearch='='
            }
            if(ColF.type==COMMON_Filter_TYPES.DATE){
                ColF.specificSearch='<='
            }


            return [Col, ColF];

        } else {
            let keyMany = ""
            let LgMany = ""

            for (let j = 0; j < def.length; j++) {
                let lgValue = def.controls[j].get("lg").value;
                if (lgValue == "fr") {
                    lgValue = "Français"
                }
                if (lgValue == "en") {
                    lgValue = "Arabe"
                }
                if (lgValue == "ar") {
                    lgValue = "Anglais"
                }

                let contentValue = def.controls[j].get("content").value;
                keyMany = keyMany + "\n" + contentValue
                LgMany = LgMany + "\n" + lgValue
                Col.type = column.get("type").value;
                Col.isSortable = column.get("isSortable").value;

            }
            Col.key = keyMany;
            Col.lg = LgMany
            ColF.key = keyMany
            ColF.lg = LgMany
            ColF.def = def
            ColF.labels = labels


            return [Col, ColF];


        }
    }

    setFiltreColumns() {

        this.responseColFiltre.payload = []

        let columns = this.formDaTab.get("columns") as FormArray;
        let fields = this.formFilter.get("fields") as FormArray;

        for (let i = 0; i < columns.length; i++) {
            let Col = this.setColumn(columns.controls[i])[0]

            this.responsePayloadCol.payload.push(Col)

        }
        for (let i = 0; i < fields.length; i++) {
            let Col1 = this.setColumn(columns.controls[i])[1]

            this.responseColFiltre.payload.push(Col1)

        }


    }


    setMappValue(tab) {
        const val = tab.className;
        let nom = val.substring(1)
        this.mapping = "t" + nom + "/data"
        this.formInfo.get('mapping').setValue(this.mapping)
    }

    deleteColumn(i) {
        this.columns.removeAt(i);
        this.setColumnsValues()

    }

    get columns() {
        return this.formDaTab.get('columns') as FormArray
    }

    get fields() {
        return this.formFilter.get('fields') as FormArray

    }

    getcontrol(formArray, k, key) {


        return formArray.controls[k].get(key) as UntypedFormControl;
    }


    onAddColonneFilter() {
        this.openFilterColumn(null, 0)

    }

    deleteFilterColumn(element, i) {
        this.fields.removeAt(i)
        this.setFiltreColumns()

    }
}
