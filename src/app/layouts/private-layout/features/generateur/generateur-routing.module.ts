import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProjetComponent } from "@privateLayout/features/generateur/projet/projet.component";
import { GestionFrontComponent } from "@privateLayout/features/generateur/gestion-front/gestion-front.component";
import { HomeComponent } from "@privateLayout/features/generateur/home/home.component";
import { DetailFrontComponent } from "@privateLayout/features/generateur/detail-front/detail-front.component";
import {
  FicheDataTableComponent
} from "@privateLayout/features/generateur/fiche-data-table/fiche-data-table.component";
import { FileViewerComponent } from "@privateLayout/features/generateur/detail-front/file-viewer/file-viewer.component";
import {
  GestionBackReverseComponent
} from "@privateLayout/features/generateur/gestion-back-reverse/gestion-back-reverse.component";
import { GestionBackComponent } from "@privateLayout/features/generateur/gestion-back/gestion-back.component";
import {AppRunComponent} from "@privateLayout/features/generateur/app-run/app-run.component";
import {TestOussComponent} from "@privateLayout/features/generateur/test-ouss/test-ouss.component";
import {BackResumeComponent} from "@privateLayout/features/generateur/back-resume/back-resume.component";
import {FicheDetailComponent} from "@privateLayout/features/generateur/fiche-detail/fiche-detail.component";

const routes: Routes = [
  {
    path: "",

    children: [

      {
        path: "home",
        data: {
          breadcrumb: "Acceuil"
        },
        component: HomeComponent
      },


      {
        path: "projet",
        data: {
          breadcrumb: "Création d'un projet"
        },
        component: ProjetComponent
      },

      {
        path: "front",
        data: {
          breadcrumb: "Front"
        },
        component: GestionFrontComponent
      },
      {
        path: 'back',
        component:GestionBackComponent,
        data: {
          breadcrumb: "Configuration Backend"
        },

      },
      {
        path: 'back-reverse',
        component:GestionBackReverseComponent,
        data: {
          breadcrumb: "Reverse engineering "
        },
      },
      {
        path: 'app-run',
        component:AppRunComponent
      },

      {
        path: "details/:idProjet",
        data: {
          breadcrumb: "Detail Application Front"
        },
        component: DetailFrontComponent
      },
      {
        path: "details-back/:id",
        data: {
          breadcrumb: "Detail Microservie"
        },
        component: BackResumeComponent
      },
      {
        path: "file",
        data: {
          breadcrumb: "lll"
        },
        component: FileViewerComponent
      },

      {
        path: "dataTable/:idProjet",
        data: {
          breadcrumb: "Géneration Fiche Data Table"
        },
        component: FicheDataTableComponent
      },
      {
        path: "fichedetail/:idProjet",
        data: {
          breadcrumb: "Géneration Fiche Data Table"
        },
        component: FicheDetailComponent
      }
    ]
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateurRoutingModule {
}
