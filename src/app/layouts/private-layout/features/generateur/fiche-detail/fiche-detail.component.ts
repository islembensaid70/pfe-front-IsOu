import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { controllerDetail, controllerMappingDetail, selectedMs, selectedOrient, selectedTab } from '@app/layouts/private-layout/shared/constantes/generateur/metadata';
import { GenerateurService } from '@app/layouts/private-layout/shared/services/generateur.service';
import { ConstanteWs } from '@app/shared/constantes/ConstanteWs';
import { Icons } from '@app/shared/constantes/Icons';
import { Pagination, RequestObject, SearchObject } from '@app/shared/models';
import { ToastService } from '@app/shared/services';
import { SharedService } from '@app/shared/services/sharedWs/shared.service';
import { onAction } from '@app/shared/tools';
import { Subscription } from 'rxjs';
import { ColumnDetailComponent } from '../fiche-data-table/column-detail/column-detail.component';
import { ColumnFicheDetailComponent } from './column-fiche-detail/column-fiche-detail.component';

@Component({
  selector: 'app-fiche-detail',
  templateUrl: './fiche-detail.component.html',
  styleUrls: ['./fiche-detail.component.css']
})
export class FicheDetailComponent implements OnInit {
  onAction = onAction;
  formDaTab: FormGroup;
  compInfo: FormGroup;

  listOrien = [{ex: "landscape"}, {ex: "portrait"}];
  params = {};
  responsePayload: any = {};
  idProjet;
  listMs: any;
  selectedMs = selectedMs;
  formInfo: FormGroup;
  entities: any;
  selectedTab = selectedTab;
  selectedOrient = selectedOrient;
  selectedEntity: String;
  node: any;
  selectedms: Subscription;
  ms: any;
  responsePayloadCol: any = {code: 200, payload: []};
  searchObject: SearchObject; 
  controllers: any = [];
  mappings: any = [];
  controllerDetail = controllerDetail;
  controllerMappingDetail = controllerMappingDetail;
  attributesEnt: any;
  attributes: any;
  mapConstWs: any;
  cleMs: any;
  displayedColumns = [];
  mapping;
  Icons=Icons;


  constructor(private formBuilder: UntypedFormBuilder,
              private generateurService: GenerateurService,
              private sharedService: SharedService, private toast: ToastService,
              private router: Router, private activatedRoute: ActivatedRoute) {
    // this.route.queryParams.subscribe(params => {
    //   if (params["data"]) {
    //     const data = JSON.parse(params["data"]);
    //     console.log(params);
    //     console.log(data);
    //     this.params = data;
    //   } else
    //     console.log("fd");
    // });

    this.idProjet = this.activatedRoute.snapshot.params.idProjet;
    this.initMs();
    this.displayedColumns = [ "key","lg", "type", "delete"];

  }

  ngOnInit(): void {
    this.searchObject = this.initSearchObject();

    this.node = this.generateurService.node;
    console.log("node", this.node);
    this.initFormComp();
    this.initformDaTab();
    this.initFormInfo();
    //this.initTables()


    this.generateurService.formDataTable = this.formDaTab;
    this.getconstantesWs();


  }

  initSearchObject() {
    const searchObject = new SearchObject();
    searchObject.pagination = new Pagination(0, 10);
    console.log(searchObject);
    return searchObject;
  }

  onPaginateTableColonnes(pagination: Pagination) {
    this.searchObject.pagination = pagination;
    //this.setColumnsValues();
  }

  initFormInfo() {
    this.formInfo = this.formBuilder.group({
      microservice: this.formBuilder.control(null, Validators.required),
      entite: this.formBuilder.control(null, Validators.required),
      controlleur: this.formBuilder.control(null, Validators.required),
      mapping: this.formBuilder.control(null, Validators.required)

    });

  }

  initFormComp() {
    this.compInfo = this.formBuilder.group({
      nomComp: this.formBuilder.control("", [Validators.required,  Validators.pattern(/^[a-zA-Z\-]+$/)
      ]),
      pathComp: this.formBuilder.control("", Validators.required)
    });

  }


  getFormControl(form, key) {
    return form.get(key) as UntypedFormControl;
  }

  async initlangue() {

    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getProjLang/",
      params: {
        path: [this.idProjet]

      },
      method: ConstanteWs._CODE_POST
    };
    return new Promise<void>((resolve, reject) => {
      this.sharedService.commonWs(request).subscribe({
        next: (response) => {
          if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

            this.responsePayload = response.payload;
            console.log("dataa langue projet ", this.responsePayload);
            resolve();


          } else {
            console.error(
              `Error in /initLangueProjet, error code :: ${response.code}`
            );
            this.toast.error();
            reject();
          }
        },
        error: (error) => {
          console.error(
            `Error in ColumnComponent/initLangueProjet, error :: ${error}`
          );
          this.toast.error();
          reject();

        }
      });


    });

  }

  initMs() {
    const request: RequestObject = <RequestObject>{
      uri: "tMicroservice/prj/",
      params: {
        path: [this.idProjet]
      },
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.listMs = response.payload;
          console.log("dataa MS", this.listMs);


        } else {
          console.error(
            `Error in FicheDataTableComponent/initMs, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in FicheDataTableComponent/initMs, error :: ${error}`
        );
        this.toast.error();
      }
    });
  }

  initTables(selectedMs) {
    console.log("fg,kjjh", selectedMs.pathMs);
    this.selectedTab.disabled = false;
    const request: RequestObject = <RequestObject>{
      uri: "tProjet/InfoClass",
      params: {
        query:
          {
            directoryBack: selectedMs.pathMs.replaceAll("\\", "/")
          }
      },
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
          console.log(response);

          this.entities = response.payload;
          console.log("dataa tables", this.entities);


        } else {
          console.error(
            `Error in FicheDataTableComponent/initTables, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in FicheDataTableComponent/initTables, error :: ${error}`
        );
        this.toast.error();
      }
    });
  }


  initControllers(selectedMs) {


    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getAllControllers",
      params: {
        query: {DirectoryPath: selectedMs.pathMs.replaceAll("\\", "/")}
      },
      method: ConstanteWs._CODE_POST
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
          console.log(response);

          this.controllers = response.payload;
          // for (let i=0;i<this.controllers.length;i++){
          //   this.mappings.push(this.controllers[i]['mappings'])
          //
          // }
          // this.mappings=[].concat.apply([], this.mappings);

          console.log("dataa initControllers", this.controllers);


        } else {
          console.error(
            `Error in FicheDataTableComponent/initControllers, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in FicheDataTableComponent/initControllers, error :: ${error}`
        );
        this.toast.error();
      }
    });
  };


  initMappingsCont(selectedController) {


    let classPath = selectedController["classPath"].replaceAll("\\", "/");
    let className = selectedController["className"];
    let classPackage = selectedController["classPackage"];

    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getOneContMappigs",
      params: {
        body: {
          classPath: classPath,
          className: className,
          classPackage: classPackage

        }
      },
      method: ConstanteWs._CODE_POST
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
          console.log(response);

          this.mappings = response.payload["mappings"];


          console.log("dataa mappings", this.mappings);


        } else {
          console.error(
            `Error in FicheDataTableComponent/initMappingsCont, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in FicheDataTableComponent/initControllers, error :: ${error}`
        );
        this.toast.error();
      }
    });


  }


  initEntCont(selectedMs) {
    console.log("elet selected", selectedMs);
    this.initTables(selectedMs);
    //  this.initControllers(selectedMs);
    this.getkeymsfromfile(selectedMs);
  }

  initformDaTab() {
    this.formDaTab = this.formBuilder.group({
      title: this.formBuilder.control(""),
      columns: this.formBuilder.array([])
    });


  }

  async setAttributes() {
    console.log("this.selectedEntity", this.selectedEntity)

    let classPath = this.selectedEntity["classPath"];
    let className = (this.selectedEntity)["className"];
    let classPackage = this.selectedEntity["classPackage"];

    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getClassAttributes",
      params: {
        body: {
          classPath: classPath,
          className: className,
          classPackage: classPackage,

        }
      },
      method: ConstanteWs._CODE_POST

    };
    return new Promise<void>((resolve, reject) => {

      this.sharedService.commonWs(request).subscribe({
        next: (response) => {
          if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
            console.log(response);

            this.attributesEnt = response.payload;


            console.log("dataa attributes11", this.attributesEnt);
            this.attributes = [];
            for (let i = 0; i < this.attributesEnt.length; i++) {
              const att = this.attributesEnt[i];
              this.attributes.push({att});

            }
            //this.attributes.push({att: "Action"});
            console.log("dataa attributes", this.attributes);
            resolve();

          } else {
            console.error(
              `Error in FicheDataTableComponent/initMappingsCont, error code :: ${response.code}`
            );
            this.toast.error();
            reject();

          }
        },
        error: (error) => {
          console.error(
            `Error in FicheDataTableComponent/initControllers, error :: ${error}`
          );
          this.toast.error();
          reject();

        }
      });


    });


  }

  async openColumnDialog() {
    {

      await this.initlangue();
      await this.setAttributes();

      this.sharedService.openDialog(ColumnFicheDetailComponent
        , {
          formDaTab: this.formDaTab,
          params: this.params,
          listLangues: this.responsePayload,
          entites: this.entities,
          selectedEntity: this.selectedEntity,
          responsePayloadCol: this.responsePayloadCol,
          attributes: this.attributes
        }).subscribe(async (response) => {
        console.log("dialog closed");
        this.setColumnsValues();


      });
    }

  }


  getconstantesWs() {
    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getMapConstanteMS/",
      params: {
        path: [this.idProjet]

      },
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.mapConstWs = response.payload;
          console.log("mapConstWs ", (this.mapConstWs));


        } else {
          console.error(
            `Error in /getconstantesWs, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in ColumnComponent/getconstantesWs, error :: ${error}`
        );
        this.toast.error();

      }
    });


  }


  getkeymsfromfile(selectedms) {

    const result = Object.entries(this.mapConstWs).find(([key, val]) => val === selectedms.nomMs);
    console.log("rhgygtr", result[0]);
    this.cleMs = result[0];

    return result[0];


  }

  getForm() {
    const val = this.formInfo.get("entite").value["className"];
    const valRef = "Details" + val;
    const myControl = this.formBuilder.control(valRef);
    this.formDaTab.addControl("ref", myControl);
    console.log("cles", this.cleMs);
    let nom = val.substring(1)
    this.mapping = "t" + nom + "/"
    this.formDaTab.addControl("uri", this.formBuilder.control(this.mapping));
    this.formDaTab.addControl("microservice", this.formBuilder.control(this.cleMs));
    console.log("rgeg", this.formInfo.get("microservice").value);
    console.log("formDaTab", this.formDaTab.value);

    const tcomposantt = {
      idProjet: this.idProjet,
      nomComp: this.compInfo.get("nomComp").value,
      routeComp: this.compInfo.get("pathComp").value
    };
    console.log("form", this.formDaTab.value);
    console.log("form", this.formDaTab.getRawValue());
    const request: RequestObject = <RequestObject><unknown>{
      uri: "tComposant/generateDetails",
      params: {

        body: {
          fileNode: this.node,
          tcomposant: tcomposantt,
          formData: this.formDaTab.getRawValue()

        }

      },
      method: ConstanteWs._CODE_POST
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.responsePayload = response.payload;
          console.log(this.responsePayload);
          this.toast.success("Composant Fiche Details  " + this.compInfo.get("nomComp").value + "  géneré avec succés");
          this.formDaTab.reset();
          this.responsePayloadCol = []
          this.formInfo.reset();
          this.compInfo.reset();
          this.router.navigate(['app/gen/details', this.idProjet])

        } else {
          console.error(
            `Error in ComposantComponent/ComposantModule, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in ModuleComponent/postModule, error :: ${error}`
        );
        this.toast.error();
      }
    });


  }

  onAdd() {


    this.selectedEntity = this.formInfo.get("entite").value;
    this.openColumnDialog();

  }


  setColumnsValues() {
    this.responsePayloadCol.payload = []
    let columns = this.formDaTab.get("columns") as FormArray;

    for (let i = 0; i < columns.length; i++) {
      let Col = this.setColumn(columns.controls[i])
      this.responsePayloadCol.payload.push(Col)

    }


    console.log("response from dat.ts", this.responsePayloadCol.payload);
  }

  setColumn(column: AbstractControl) {
    let def = column.get("def") as FormArray;
    let key = "";
    let Col = {
      key: "",
      type: "",
      lg: ""
    };


    if (def.length == 1) {
      key = def.controls[0].get("content").value;
      Col.key = key;
      Col.type = column.get("type").value;
      Col.lg = "Unilingue"
      return Col;

    } else {
      let keyMany = ""
      let LgMany = ""

      for (let j = 0; j < def.length; j++) {
        let lgValue = def.controls[j].get("lg").value;
        if (lgValue == "fr") {
          lgValue = "Français"
        }
        if (lgValue == "en") {
          lgValue = "Arabe"
        }
        if (lgValue == "ar") {
          lgValue = "Anglais"
        }

        let contentValue = def.controls[j].get("content").value;
        keyMany = keyMany + "\n" + contentValue
        LgMany = LgMany + "\n" + lgValue
        Col.type = column.get("type").value;

      }
      Col.key = keyMany;
      Col.lg = LgMany

      return Col;


    }
  }


  setMappValue(tab) {
    console.log("tabb", tab.className)
    const val = tab.className;
    let nom = val.substring(1)
    this.mapping = "t" + nom + "/{id}"
    this.formInfo.get('mapping').setValue(this.mapping)
  }

  deleteColumn(i) {
    console.log("indexxx",i)
    this.columns.removeAt(i);
    this.setColumnsValues()

  }
  get columns(){
    return this.formDaTab.get('columns') as FormArray
  }

}
