import { Component, Inject, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  UntypedFormArray,
  UntypedFormControl,
  Validators, ɵElement, ɵFormGroupRawValue, ɵGetProperty, ɵTypedOrUntyped
} from "@angular/forms";
import { SharedService } from "@shared/services/sharedWs/shared.service";
import { ToastService } from "@shared/services";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { RequestObject, SearchObject, SelectMetadata } from "@shared/models";
import { COMMON_TYPES_CODES_FICHE_DETAILS } from "@shared/constantes/Constante";
import {
  entityMetadata,
  iconsMetadata,
  selectedLangue,
  typeCol
} from "@privateLayout/shared/constantes/generateur/metadata";
import { concat } from "rxjs";
import { ConstanteWs } from "@shared/constantes/ConstanteWs";

@Component({
  selector: 'app-column-fiche-detail',
  templateUrl: './column-fiche-detail.component.html',
  styleUrls: ['./column-fiche-detail.component.css']
})
export class ColumnFicheDetailComponent implements OnInit {
  formDaTab: FormGroup;
  formColumn: FormGroup;
  listLang: any;
  searchObject: SearchObject;
  params = {};
  data: any;
  selectLangue: SelectMetadata;
  selectedEntity: String;
  attributes: any;
  entityMetadata = entityMetadata;
  iconsMetadata = iconsMetadata;
  typeCol = typeCol;
  selectedLang = [];
  selectedContent = [];
  COMMON_TYPES_CODESS = Object.keys(COMMON_TYPES_CODES_FICHE_DETAILS).map(key => ({ key, value: COMMON_TYPES_CODES_FICHE_DETAILS[key] }));

  responsePayloadCol: any = {};
  attributesEnt: any;

  constructor(private sharedService: SharedService,
              private toast: ToastService,
              private _formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ColumnFicheDetailComponent, { data: any }>,
              @Inject(MAT_DIALOG_DATA) data) {

    this.formDaTab = data.formDaTab;
    this.params = data.params;
    this.listLang = data.responsePayload;
    this.selectedEntity = data.selectedEntity;
    this.selectLangue = selectedLangue;
    this.responsePayloadCol = data.responsePayloadCol;
    this.data = data;
    this.attributes=data.attributes;

    console.log("data", data);

  }

  ngOnInit(): void {

    // this.setAttributes();
    this.initFormCol();


    console.log("formDaTab", this.formDaTab);


  }

  setAttributes() {

    let classPath = this.selectedEntity["classPath"].replaceAll("\\", "/");
    let className = (this.selectedEntity)["className"];
    let classPackage = this.selectedEntity["classPackage"];

    const request: RequestObject = <RequestObject>{
      uri: "tProjet/getOneClassAttributes",
      params: {
        body: {
          classPath: classPath,
          className: className,
          classPackage: classPackage

        }
      },
      method: ConstanteWs._CODE_POST
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
          console.log(response);

          this.attributesEnt = response.payload['attributes'];


          console.log("dataa attributes11", this.attributesEnt);
          this.attributes =[]
          for(let i = 0; i < this.attributesEnt.length; i++) {
            const att = this.attributesEnt[i];
            this.attributes.push({att})

          }
          this.attributes.push({ att: "Action" });
          console.log("dataa attributes", this.attributes);

        } else {
          console.error(
            `Error in FicheDataTableComponent/initMappingsCont, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in FicheDataTableComponent/initControllers, error :: ${error}`
        );
        this.toast.error();
      }
    });





  }

  onPrecedent() {
    this.dialogRef.close({ data: {column:" column dialog closed" }});
  }

  getFormControl(key) {
    return this.formColumn.get(key) as UntypedFormControl;
  }

  get def() {
    return this.formColumn.get("def") as UntypedFormArray;
  }

  get labels() {
    return this.formColumn.get("labels") as UntypedFormArray;

  }



  setlabelControls() {

  }

  initFormCol() {
    const itemControls = [];
    for (let i = 0; i < this.data['listLangues'].length; i++) {
      let formGroup = this._formBuilder.group({
        labelg: this._formBuilder.control(this.data["listLangues"][i]["labelLang"]),
        keylg: this._formBuilder.control(this.data["listLangues"][i]["keyLang"]),

        content: this._formBuilder.control(null, [Validators.required])
      });
      itemControls.push(formGroup);
    }

    console.log("itemscontrols", itemControls);


    this.formColumn = this._formBuilder.group({
        unilang: this._formBuilder.control(true),
        def: this._formBuilder.array([
          this._formBuilder.group({

            lg: this._formBuilder.control(null, [Validators.required]),
            content: this._formBuilder.control(null, [Validators.required])
          })]),
        labels: this._formBuilder.array(itemControls),


        type: this._formBuilder.control(COMMON_TYPES_CODES_FICHE_DETAILS["TEXT"], [Validators.required]),

      }
    );
    console.log("form after init", this.formColumn);


  }


  ajouterCol() {
    (<FormArray>this.formDaTab.get("columns")).push(this.formColumn);
    //this.setColumnsValues();
    this.onPrecedent();

  }

  get columns() {
    return this.formDaTab.get("columns") as FormArray;

  }

  getcontrol(formArray, k, key) {
    return formArray.controls[k].get(key) as UntypedFormControl;
  }

  addLangCol() {

    const langControl = this._formBuilder.group({
      lg: this._formBuilder.control(null),
      content: this._formBuilder.control(null)


    });
    this.setLgCont(langControl.get("lg"), langControl.get("content"));

    (<FormArray>this.formColumn.get("def")).push(langControl);

  }



  delLangCol(k: number) {

    this.def.removeAt(k);
    // this.setLgCont();

  }

  setLgCont(lgControl: AbstractControl<ɵGetProperty<ɵTypedOrUntyped<{ [K in keyof { lg: FormControl<unknown | null>; content: FormControl<unknown | null> }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null> }, ɵFormGroupRawValue<{ [K in keyof { lg: FormControl<unknown | null>; content: FormControl<unknown | null> }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null> }>, any>, "lg">>, contentControl: AbstractControl<ɵGetProperty<ɵTypedOrUntyped<{ [K in keyof { lg: FormControl<unknown | null>; content: FormControl<unknown | null> }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null> }, ɵFormGroupRawValue<{ [K in keyof { lg: FormControl<unknown | null>; content: FormControl<unknown | null> }]: ɵElement<{ lg: FormControl<unknown | null>; content: FormControl<unknown | null> }[K], null> }>, any>, "content">>) {
    this.def.controls.forEach((control) => {
      this.selectedLang.push(control.getRawValue().lg);
      this.selectedContent.push(control.getRawValue().content);
    });


    let lg = lgControl.value;
    // console.log("before");
    // console.log("selectedlang", this.selectedLang);
    // console.log("selectedContent", this.selectedContent);
    this.selectedContent.forEach((selectedValue: string) => {
      this.attributes = this.attributes.filter((option: any) => option.att !== selectedValue);
    });
    lgControl.patchValue(lg);


    let cont = contentControl.value;
    this.selectedLang.forEach((selectedValue: string) => {
      this.data["listLangues"] = this.data["listLangues"].filter((option: any) => option.keyLang !== selectedValue);
    });
    contentControl.patchValue(cont);

    // console.log("after");
    // console.log("attribures", this.attributes);
    // console.log("data['listLangues']", this.data["listLangues"]);


  }

  setColumnsValues() {

    let datacol = [];
    let allkeys = "";
    let Col = {
      key: "",
      type: "",
    };
    const columns = this.formDaTab.get("columns") as FormArray;
    for (let i = 0; i < columns.length; i++) {


      const def = columns.controls[i].get("def") as FormArray;
      let key = "";
      if (def.length == 1) {
        key = def.controls[0].get("content").value;
        Col.key = key;
        Col.type = columns.controls[i].get("type").value;

      } else {

        for (let j = 0; j < def.length; j++) {
          let lgValue = def.controls[j].get("lg").value;
          let contentValue = def.controls[j].get("content").value;


          key = key + "Langue: " + lgValue + " Contenu: " + contentValue;

        }

        Col.key = key;
        Col.type = columns.controls[i].get("type").value;


      }

      allkeys = allkeys + key;
      datacol.push(Col);
    }

    this.responsePayloadCol = datacol;
  }

  getcontrolArr(formArray: FormArray<any>, i: number) {

    return formArray.controls[i] as UntypedFormControl;
  }
}
