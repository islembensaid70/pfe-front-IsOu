import {Component, OnInit, ViewChild} from "@angular/core";
import {
    AbstractControl,
    FormBuilder,
    FormGroup, UntypedFormBuilder,
    UntypedFormControl, UntypedFormGroup,
    ValidationErrors,
    Validators
} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Observable, Subscription} from "rxjs";
import {error} from "protractor";
import {ToastrService} from "ngx-toastr";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatStepper} from "@angular/material/stepper";
import * as path from "path";
import {AuthentificationService, ConfirmDialogService, ToastService} from "@shared/services";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {Project} from "@privateLayout/shared/models/Project";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {ResponseObject} from "@shared/models/ResponseObject";
import {ModuleComponent} from "@privateLayout/features/generateur/module/module.component";
import {GestionFrontComponent} from "@privateLayout/features/generateur/gestion-front/gestion-front.component";

import {selectedLangues} from "@privateLayout/shared/constantes/generateur/metadata";
import {profil} from "@privateLayout/shared/constantes/generateur/metadata";

@Component({
    selector: "app-projet",
    templateUrl: "./projet.component.html",
    styleUrls: ["./projet.component.css"]
})
export class ProjetComponent implements OnInit {

    @ViewChild(MatStepper) stepper: MatStepper;
    directoryPath: string;
    labels: any;
    form: UntypedFormGroup;
    title: any;
    project: Project = new Project();
    idprojet: number;
    listLang: any;
    listprofils: any;

    selectedLang = [];
    selectedProfils = [];
    langPrj = [];
    id: number;
    selectedLangues = selectedLangues;
    profil = profil;
    formLg: FormGroup;
    lang: UntypedFormControl
    prof: UntypedFormControl


    constructor(private toastr: ToastService, private _snackBar: MatSnackBar
        , private http: HttpClient, private generateurService: GenerateurService, private activatedRoute: ActivatedRoute,
                private router: Router,
                private sharedService: SharedService,
                private toast: ToastService,
                private formBuilder: UntypedFormBuilder,
                private authentifiSer:AuthentificationService
    ) {


    }

    ngOnInit(): void {
        this.initForm();
        this.initlangue();
        this.initFormLg()
        this.initFormPorfil()
        this.initProfils()


    }

    /**Création du projet*/
    createProject(project) {
        console.log("project");

        console.log(project);
        const requete: RequestObject = <RequestObject>{
            uri: "tProjet/",
            method: ConstanteWs._CODE_POST,
            params: {
                body: project
            }
        };
        this.sharedService.commonWs(requete).subscribe({

            next: (response: ResponseObject) => {

                if (response.code === ConstanteWs._CODE_WS_SUCCESS) {
                    console.log("this.id from projet");

                    this.generateurService.id = response.payload['idProjet']
                    this.idprojet = response.payload['idProjet'];
                    localStorage.setItem("idProjet", response.payload['idProjet'])

                    this.getSelectedLang()

                    console.log(this.generateurService.id);


                    console.log(<[]>response.payload);
                    this.router.navigate(["/app/gen/back"]);

                    //this.openDialog()
                } else {
                    console.error("Error in post 1 projet--response");
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error("Error in post 2 projet");
                this.toast.error();
            }
        });


    }

    /**Création du workspace*/

    initDirectory() {

        this.generateurService.getDesktopPath().subscribe(data => {
            this.directoryPath = data;

            console.log(this.directoryPath);
        });
    }


    /**Init form Langue*/
    initFormLg() {
        // this.form = this._formBuilder.group({
        this.lang = this.formBuilder.control(null, Validators.required)

    }

    /**Init form profil*/
    initFormPorfil() {
        // this.form = this._formBuilder.group({
        this.prof = this.formBuilder.control(null, Validators.required)
    }

    /**Recuperer les langues selectionnes*/
    getSelectedLang() {
        console.log("fef", this.selectedLang);
        this.selectedLang = this.lang.value;
        this.sendPrjLangues();
        //this.runnpmInstall()
        console.log(this.selectedLang);
    }

    /**Recuperer les profils selectionnes*/
    getSelectedProfils(profil) {
        console.log("aaaa ", profil)

        this.selectedProfils = this.prof.value;
        console.log(this.selectedProfils);
        const myListString = JSON.stringify(this.selectedProfils);

// Save the string in local storage
        localStorage.setItem('selectedProfils', myListString);
    }

    getFormControl(key) {
        return this.form.get(key) as UntypedFormControl;
    }


    /**Post des langues relatives au projet */
    sendPrjLangues() {
        console.log("id from gestion front");
        console.log(this.id);
        if (this.selectedLang) {
            for (let i = 0; i < this.selectedLang.length; i++) {
                const body = {};
                body["idPrj"] = this.idprojet;

                body["idLang"] = this.selectedLang[i];
                this.langPrj.push(body);


            }
            console.log("array");

            console.log(this.langPrj);
            const request: RequestObject = <RequestObject>{

                uri: "tProjetLangue/lgp/",
                method: ConstanteWs._CODE_POST,
                params: {
                    body: this.langPrj
                }
            };
            this.sharedService.commonWs(request).subscribe({
                next: (response: ResponseObject) => {
                    if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                        console.log("payload lang projet ");
                        console.log(<[]>response.payload);
                        this.toast.success("Langues ajoutées au projet avec sucées");
                        this.toast.success("Fichiers i18n crées");

                        //this.router.navigate(["/app/gen/back", this.id]);


                    } else {
                        console.error("Error in post lang projet")
                        ;
                        this.toast.error();
                    }
                },
                error: (error) => {
                    console.error("Error in post lang projet");
                    this.toast.error();
                }
            });
        }


    }

    /**Validation du form*/
    validateForm() {
        if (this.form.valid) {
            if ((this.form.get('nomAppB').value == this.form.get('nomAppF').value))
                return false
            else
                return true
        }

    }


    /**Init form */
    initForm() {
        this.initDirectory();
        this.form = this.formBuilder.group({
            nomProjet: this.formBuilder.control("", Validators.required),
            nomAppB: this.formBuilder.control("", Validators.required),
            nomAppF: this.formBuilder.control("", Validators.required)
        }, {matchingBackAndFrontNamesValidator});

        function matchingBackAndFrontNamesValidator(form: FormGroup) {
            let nomAppB = form.get("nomAppB")?.value;
            let nomAppF = form.get("nomAppF")?.value;

            if (form.get("nomAppB")?.value == form.get("nomAppF")?.value) {
                return {matchingBackAndFrontNames: false};
            } else {
                return true;
            }
        }


    }


    changeColorP(colorPicked) {

        this.project.couleurPrim = colorPicked.color.hex;
    }

    changeColorS(colorPicked) {

        this.project.couleurSideB = colorPicked.color.hex;
    }

    changeColorN(couleurNavB) {


        this.project.couleurNavB = couleurNavB.color.hex;
    }

    /** Creation du work projet clone front*/
    createDirectory() {
        this.generateurService.createDirectory
        (this.directoryPath, this.getFormControl("nomProjet").value).subscribe(
            (response) => {
                (this.generateurService.createDirectory(this.directoryPath + "/" +
                    this.getFormControl("nomProjet").value.trim(),
                    this.getFormControl("nomAppB").value.trim()).subscribe((response) => {
                    localStorage.setItem("backEndPath", this.directoryPath + "/" +
                        this.getFormControl("nomProjet").value.trim() + "/" +
                        this.getFormControl("nomAppB").value.trim());
                }),
                    this.generateurService.createDirectory(this.directoryPath + "/" +
                        this.getFormControl("nomProjet").value.trim(),
                        this.getFormControl("nomAppF").value.trim()).subscribe((response) => {
                        this.project.emplProjet = this.directoryPath + "/" + this.getFormControl("nomProjet").value.trim();
                        this.project.nomProjet = this.getFormControl("nomProjet").value;
                        this.project.nomAppB = this.getFormControl("nomAppB").value;
                        this.project.nomAppF = this.getFormControl("nomAppF").value;
                        if (this.project.nomProjet.endsWith(" ")) {
                            this.project.nomProjet = this.project.nomProjet.trimEnd()
                        }
                        if (this.project.nomAppB.endsWith(" ")) {
                            this.project.nomAppB = this.project.nomAppB.trimEnd()
                        }
                        if (this.project.nomAppF.endsWith(" ")) {
                            this.project.nomAppF = this.project.nomAppF.trimEnd()
                        }
                        console.log("this.form.value");
                        console.log(this.project);
                        this.project.idUser=this.authentifiSer.authenticatedUser['id_user']

                        this.createProject(this.project);

                    }));
            }, (error) => {
                if (error?.status === 400) {
                    // Error toast
                    this.toastr.error("Le nom du répertoire du projet existe déjà", {
                        classname: "bg-danger text-light",
                        delay: 3000
                    });
                } else {
                    // Error toast for other errors
                    this.toastr.error("Erreur lors de la création du répertoire", {
                        classname: "bg-danger text-light",
                        delay: 3000
                    });
                }
            }
        );


    }

    /**Recuperer tous les langues existantes*/
    async initlangue() {
        this.listLang = await this.sharedService.getListNm("LIST_LANGUE");

        console.log("list avec getlist nm", this.listLang);
    }

    /**Init profils*/
    initProfils() {
        const request: RequestObject = <RequestObject>{
            uri: "tProfile/",
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response: ResponseObject) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.listprofils = <[]>response.payload;


                    console.log(this.listprofils, ' liste des profils ');

                } else {
                    console.error("Error in listMsType")
                    ;
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error("Error in listMsType");
                this.toast.error();
            }
        });
    }

    /**Executer npm install pour le projet front*/

    runnpmInstall() {
        const request: RequestObject = <RequestObject>{
            uri: "tProjet/runNpmInstall/",
            params: {
                path: [this.idprojet]

            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    console.log(response);


                } else {
                    console.error(
                        `Error in /getconstantesWs, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ColumnComponent/getconstantesWs, error :: ${error}`
                );
                this.toast.error();

            }
        });
    }


}

