import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, UntypedFormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { SharedService } from "@shared/services/sharedWs/shared.service";
import { ToastService } from "@shared/services";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { RequestObject } from "@shared/models";
import { ConstanteWs } from "@shared/constantes/ConstanteWs";
import { MatTreeNestedDataSource } from "@angular/material/tree";
import { NestedTreeControl } from "@angular/cdk/tree";

interface FileNode {
  name: string;
  type: string;
  module: string;
  children?: FileNode[];
}


@Component({
  selector: "app-module",
  templateUrl: "./module.component.html",
  styleUrls: ["./module.component.css"]
})
export class ModuleComponent implements OnInit {
  idProjet: number;
  responsePayload: any = {};
  node: any;
  indexNode;

  dataSource = new MatTreeNestedDataSource<FileNode>();

  treeControl = new NestedTreeControl<FileNode>(node => node.children);

  constructor(
    private activatedRoute: ActivatedRoute,
    private sharedService: SharedService,
    private toast: ToastService,
    private _formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModuleComponent, { data: any }>,
    @Inject(MAT_DIALOG_DATA) data) {

    this.idProjet = data.idProjet;
    this.node = data.node,
      this.dataSource = data.dataSource;
    this.treeControl = data.treeControl;
    this.indexNode=data.index;
  }

  formM: FormGroup;

  ngOnInit(): void {
    this.initFormM();
  }

  getFormControl(key) {
    return this.formM.get(key) as UntypedFormControl;
  }

  initFormM() {
    this.formM = this._formBuilder.group({
        nomModule: this._formBuilder.control("", Validators.required),
        routeModule: this._formBuilder.control("", Validators.required)

      }
    );
  }

  onPrecedent() {
    this.dialogRef.close();

  }

  initArchProjet() {
    const request: RequestObject = <RequestObject><unknown>{
      uri: "tProjet/getArch/",
      params: {
        path: [this.idProjet]
      },
      method: ConstanteWs._CODE_GET
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.responsePayload = response.payload;


          this.dataSource.data = this.responsePayload;
          console.log("dataa projet arch", this.responsePayload);

        } else {
          console.error(
            `Error in ModuleComponent/initArchProjet, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in ModuleComponent/initArchProjet, error :: ${error}`
        );
        this.toast.error();
      }
    });
  }

  onSaveModule() {
    console.log("idProject");
    console.log(this.idProjet);

    console.log("node detail", this.node);
    this.node.children = null;


    const request: RequestObject = <RequestObject><unknown>{
      uri: "tModule/",
      params: {

        body: {
          fileNode: this.node,
          tmodule: {
            nomModule: this.formM.get("nomModule").value,
            routeModule: this.formM.get("routeModule").value,
            idProjet: this.idProjet
          }


        }

      },
      method: ConstanteWs._CODE_POST
    };
    this.sharedService.commonWs(request).subscribe({
      next: (response) => {
        if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

          this.responsePayload = response.payload;
          console.log("Payload module",this.responsePayload);
          this.toast.success("Module   "+this.formM.get("nomModule").value+"  géneré avec succés")
          this.dialogRef.close({data: 'native close'});


        } else {
          console.error(
            `Error in ModuleComponent/postModule, error code :: ${response.code}`
          );
          this.toast.error();
        }
      },
      error: (error) => {
        console.error(
          `Error in ModuleComponent/postModule, error :: ${error}`
        );
        this.toast.error();
      }
    });

  }
}
