import {CriteriaSearch, Pagination, RequestObject, SearchObject} from "@shared/models";
import {onAction} from "@shared/tools";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {AuthentificationService, ToastService} from "@shared/services";
import {filtreProjetGen, tableProjet} from "@privateLayout/shared/constantes/generateur/metadata";
import {Router} from "@angular/router";
import {Component, OnInit} from "@angular/core";

import {getDataSelect, onSelectFilter} from "@shared/tools/utils/filter-factory";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {

    onAction = onAction;
    onSelectFilter = onSelectFilter;
    getDataSelect = getDataSelect;
    searchObject: SearchObject;
    responsePayload: any = {};
    metadataProjet: any = {};
    metadatFiltreProjet: any = {}
    listLangues: any;
    mapSelect: Map<string, any[]> = new Map();

    constructor(private sharedService: SharedService, private toast: ToastService,
                private router: Router,
                private authentifiSer:AuthentificationService
    ) {
        this.initParams();
        this.initSearchObject();

    }

    async setMap(tables) {
        for (let table of tables) {
            await this.getDataSelect(table)
                .then((list: any[]) => {
                    this.mapSelect.set(table, list)
                })
                .catch((error) => {
                    console.error('Error: ', error);
                    // Handle the error here
                });
        }

    }

    ngOnInit() {
        this.searchObject = this.initSearchObject();
        this.initDataProjet();

    }

    onPaginateTableProjet(pagination: Pagination) {
        this.searchObject.pagination = pagination;
        this.initDataProjet();
    }

    initSearchObject() {
        const searchObject = new SearchObject();
        searchObject.pagination = new Pagination(0, 10);
        console.log(searchObject);
        let creteriaSearch = new CriteriaSearch("idUser", this.authentifiSer.authenticatedUser['id_user'], "=")
        searchObject.dataSearch.push(creteriaSearch)
        return searchObject;
    }

    initParams() {
        this.metadataProjet = tableProjet;
        this.metadatFiltreProjet = filtreProjetGen;

    }

    openDetailsTableProjet(row) {

        console.log("row");
        console.log(row);

        this.router.navigate(["/app/gen/details", row.item.idProjet]);
        return;

    }

    onSortTableProjet(sort: any) {
        this.searchObject.sort = sort;
        this.initDataProjet()
    }


    initDataProjet() {
        const request: RequestObject = <RequestObject>{
            uri: "tProjet/data",
            params: {
                body: this.searchObject
            },
            method: ConstanteWs._CODE_POST
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    this.responsePayload = response.payload;


                } else {
                    console.error(
                        `Error in FicheHomeComponent/initDataProjet, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in FicheHomeComponent/initDataProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });

    }


    onSearch($event) {
        this.searchObject.pagination.offSet = 0;
        this.searchObject.pagination.limit = 10;
        this.searchObject.dataSearch = $event;
        this.initDataProjet()

    }


}
