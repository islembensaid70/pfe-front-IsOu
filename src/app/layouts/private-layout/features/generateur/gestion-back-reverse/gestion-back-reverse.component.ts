import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {GenerateurService} from "@privateLayout/shared/services/generateur.service";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {Router} from "@angular/router";

@Component({
    selector: 'app-gestion-back-reverse',
    templateUrl: './gestion-back-reverse.component.html',
    styleUrls: ['./gestion-back-reverse.component.css']
})
export class GestionBackReverseComponent implements OnInit {

    tables: any = []
    listMs: any = [];
    selectedCheckboxes: string[] = [];
    selectedTables: any[] = [];
    backEndPath: string;
    url: string;
    password: string;
    username: string;
    responsePayload: any;
    idProjet: number;
    selectAll: any;


    constructor(private generateurService: GenerateurService, private sharedService: SharedService,
                private toast: ToastService,private router:Router) {
        this.idProjet = parseInt(localStorage.getItem('idProjet'))
    }


    ngOnInit(): void {
        const listMsFromLocalStorage = localStorage.getItem('listMsSentToReverse');
        this.listMs = JSON.parse(listMsFromLocalStorage);
        const listTablesFromLocalStorage = localStorage.getItem('tables');
        this.tables = JSON.parse(listTablesFromLocalStorage);
        this.backEndPath = localStorage.getItem('backEndPath');
        this.url = localStorage.getItem('url');
        this.password = localStorage.getItem('password');
        this.username = localStorage.getItem('username');
        console.log(this.tables)
        console.log(this.listMs)
    }





    reverse() {


        this.generateurService.reverseTable(this.selectedTables, this.url, this.password, this.username).subscribe()

        this.writeMsConsta()
        this.setEnvWs();
        this.router.navigate(['/app/gen/details/'+this.idProjet])

    }

    setEnvWs() {
        const request: RequestObject = <RequestObject>{
            uri: "tProjet/getMapConstanteGateway/",
            params: {
                path: [this.idProjet]

            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {


                } else {
                    console.error(
                        `Error in /getconstantesWs, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ColumnComponent/getconstantesWs, error :: ${error}`
                );
                this.toast.error();

            }
        });


    }

    writeMsConsta() {
        console.log("idd", this.generateurService.id)
        const request: RequestObject = <RequestObject><unknown>{
            uri: "tProjet/writeConstanteMS/",
            params: {
                path: [this.idProjet]
            },
            method: ConstanteWs._CODE_GET
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                    this.responsePayload = response.payload;
                    this.toast.success("Constantes  Ecrites ")


                } else {
                    console.error(
                        `Error in ModuleComponent/writeMsConsta, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in ModuleComponent/writeMsConsta, error :: ${error}`
                );
                this.toast.error();
            }
        });


    }


    onCheckboxClick(event: any, item: any, tableData: any, ms) {
        if (event.target.checked) {
            // Add the item to the selected items list
            console.log('checked');
            this.addToSelectedTables(item, tableData ,ms);
        } else {
            console.log('unchecked');
            // Remove the item from the selected items list
            this.removeFromSelectedTables(item, tableData ,ms);
        }
        console.log(this.selectedTables);
    }

    selectAllCheckbox(event: any, tableData: any, ms) {
        const items = tableData.value;

        if (event.target.checked) {
            // Select all items for the respective table
            for (const item of items) {
                this.addToSelectedTables(item, tableData, ms);
            }
        } else {
            // Deselect all items for the respective table
            for (const item of items) {
                this.removeFromSelectedTables(item, tableData, ms);
            }
        }
    }

    addToSelectedTables(item: any, tableData: any, ms) {
        const tableName = item;


        const path = this.backEndPath + '/' + (ms.labelToDisplay || ms.labelType);

        this.selectedTables.push({ tableName, path, item });
    }

    removeFromSelectedTables(item: any, tableData: any,ms) {
        const tableName = item;
        const path = this.backEndPath + '/' + (ms.labelToDisplay || ms.labelType);

        const index = this.selectedTables.findIndex(
          selectedItem =>
            selectedItem.path === path &&
            selectedItem.tableName === item &&
            selectedItem.item === item
        );
        if (index >= 0) {
            this.selectedTables.splice(index, 1);
        }
    }

    isItemSelected(tableData: any, item: any,ms): boolean {
        const tableName = tableData.key;
        const path = this.backEndPath + '/' + (ms.labelToDisplay || ms.labelType);

        return this.selectedTables.some(
          selectedItem =>
            selectedItem.path === path &&
            selectedItem.tableName === item &&
            selectedItem.item === item
        );
    }




    test() {
        console.log(this.selectedTables)
    }


    // Inside your component class
    filterCheckboxGroup(tableData: any) {
        tableData.filteredItems = tableData.value.filter(item => item.toLowerCase().includes(tableData.searchFilter.toLowerCase()));
    }

    getFilteredItems(tableData: any): any[] {
        return tableData.filteredItems || tableData.value;
    }



}
