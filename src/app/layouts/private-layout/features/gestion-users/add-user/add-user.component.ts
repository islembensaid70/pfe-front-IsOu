import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, UntypedFormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {ResponseObject} from "@shared/models/ResponseObject";

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
    formUser: FormGroup

    constructor(private formBuider: FormBuilder, private router: Router,
                private sharedService: SharedService,
                private toast: ToastService) {
    }

    ngOnInit(): void {
        this.initFormUser()
    }

    initFormUser() {
        this.formUser = this.formBuider.group({
            login: this.formBuider.control('', Validators.required),
            nom: this.formBuider.control('', Validators.required),
            prenom: this.formBuider.control('', Validators.required),
            password: this.formBuider.control('', Validators.required),
            confirmPassword: this.formBuider.control('', Validators.required),
            email: this.formBuider.control('', Validators.email)

        })

    }

    getFormControl(key) {
        return this.formUser.get(key) as UntypedFormControl;
    }

    addUser() {
        this.formUser.addControl('role', this.formBuider.control('User'))
        this.formUser.addControl('isActif', this.formBuider.control(true))

        const request: RequestObject = <RequestObject>{
            uri: "register",
            method: ConstanteWs._CODE_POST,
            params: {
                body: this.formUser.value
            }
        };

        this.sharedService.commonWs(request).subscribe({
            next: (response: ResponseObject) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    console.log(<[]>response.payload, ' <[]>response.payload tMicroservice/saveAll');
                    this.toast.success("Utilisateur crée " + this.formUser.get('nom').value +"   "+  this.formUser.get('prenom').value + "  avec succès")
                    //this code to add a idCreatedMs attribute to the list of the microservices after post"   " +
                    this.router.navigate(['/app/adm/users'])

                } else {
                    console.error("Error in post microservice ")
                    ;
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error("Error in post miscroservice");
                // localStorage.setItem('isMsCreated', 'false');
                this.toast.error();
            }
        });
    }
}



