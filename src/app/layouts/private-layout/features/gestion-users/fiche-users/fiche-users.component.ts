import {Component, OnInit} from '@angular/core';
import {Pagination, RequestObject, SearchObject} from "@shared/models";
import {onAction} from "@shared/tools";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {usersTable} from "@privateLayout/shared/constantes/gestion-users/gestion-users-metadata";
import {Router} from "@angular/router";

@Component({
    selector: 'app-fiche-users',
    templateUrl: './fiche-users.component.html',
    styleUrls: ['./fiche-users.component.css']
})
export class FicheUsersComponent implements OnInit {
    onAction = onAction;
    searchObject: SearchObject;
    responsePayload: any = {};
    usersMetadata: any;

    constructor(private sharedService: SharedService, private toast: ToastService, private router: Router) {
        this.initParams();

    }

    ngOnInit()
        :
        void {
        this.searchObject = this.initSearchObject();
        this.initDataUsers();
    }

    initSearchObject() {
        const searchObject = new SearchObject();
        searchObject.pagination = new Pagination(0, 10);
        console.log(searchObject);
        return searchObject;
    }


    initDataUsers() {
        const request: RequestObject = <RequestObject>{
            uri: "utilisateur/data",
            params: {
                body: this.searchObject
            },
            method: ConstanteWs._CODE_POST
        };
        this.sharedService.commonWs(request).subscribe({
            next: (response) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    this.responsePayload = response.payload;


                } else {
                    console.error(
                        `Error in FicheHomeComponent/utilisateur, error code :: ${response.code}`
                    );
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error(
                    `Error in FicheHomeComponent/initDataProjet, error :: ${error}`
                );
                this.toast.error();
            }
        });


    }


    initParams() {
        this.usersMetadata = usersTable

    }


    openDetailsTableUsers(row) {

        console.log("row");
        console.log(row);


    }
    onEditTableUsers(row){
        console.log("row");
        console.log(row);
        this.router.navigate(['/app/adm/upd-user/'+row.item.idAdmUser])
    }
    onAddTableUsers(){
        this.router.navigate(['/app/adm/add-user'])
    }
    onPaginateTableUsers(pagination:any){
        this.searchObject.pagination = pagination;
        this.initDataUsers();
    }

    onSortTableUsers(sort: any) {
        this.searchObject.sort = sort;
        this.initDataUsers()
    }
}
