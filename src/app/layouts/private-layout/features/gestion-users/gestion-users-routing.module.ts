import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FicheUsersComponent} from "@privateLayout/features/gestion-users/fiche-users/fiche-users.component";
import {AddUserComponent} from "@privateLayout/features/gestion-users/add-user/add-user.component";
import {UpdateUserComponent} from "@privateLayout/features/gestion-users/update-user/update-user.component";

const routes: Routes = [
    {
        path: 'users', component: FicheUsersComponent,
        // data: {
        //     breadcrumb: "Liste des utilisateurs"
        // },
    }, {
        path: 'add-user',
        component: AddUserComponent,
        // data: {
        //     breadcrumb: "Ajout d'un utilisateur"
        // },
    },
    {path: 'upd-user/:id', component: UpdateUserComponent}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GestionUsersRoutingModule {
}
