import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionUsersRoutingModule } from './gestion-users-routing.module';
import { FicheUsersComponent } from './fiche-users/fiche-users.component';
import { AddUserComponent } from './add-user/add-user.component';
import {SharedModule} from "@shared/shared.module";
import { UpdateUserComponent } from './update-user/update-user.component';


@NgModule({
  declarations: [
    FicheUsersComponent,
    AddUserComponent,
    UpdateUserComponent
  ],
    imports: [
        CommonModule,
        GestionUsersRoutingModule,
        SharedModule
    ]
})
export class GestionUsersModule { }
