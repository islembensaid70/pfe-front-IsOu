import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, UntypedFormControl, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedService} from "@shared/services/sharedWs/shared.service";
import {ToastService} from "@shared/services";
import {RequestObject} from "@shared/models";
import {ConstanteWs} from "@shared/constantes/ConstanteWs";
import {ResponseObject} from "@shared/models/ResponseObject";

@Component({
    selector: 'app-update-user',
    templateUrl: './update-user.component.html',
    styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

    id: number
    formUser: FormGroup
    dataUser:any;

    constructor(private formBuider: FormBuilder, private actroute: ActivatedRoute, private router: Router,
                private sharedService: SharedService,
                private toast: ToastService) {
        this.id = this.actroute.snapshot.params.id;
        console.log("idd",this.id)

    }

    ngOnInit(): void {
        this.initFormUser()
    }

    async initFormUser() {
        await this.inituser()

        console.log("aa",this.dataUser)
        this.formUser = this.formBuider.group({
            idAdmUser: this.formBuider.control(this.dataUser['idAdmUser'], Validators.required),

            login: this.formBuider.control(this.dataUser['login'], Validators.required),
            nom: this.formBuider.control(this.dataUser['nom'], Validators.required),
            prenom: this.formBuider.control(this.dataUser['prenom'], Validators.required),
            password: this.formBuider.control(this.dataUser['password'], Validators.required),
            email: this.formBuider.control(this.dataUser['email'], Validators.email),
            isActif: this.formBuider.control(this.dataUser['isActif']),
            role: this.formBuider.control(this.dataUser['role'])


        })

    }

    getFormControl(key) {
        return this.formUser.get(key) as UntypedFormControl;
    }

    updateUser() {
        const request: RequestObject = <RequestObject>{
            uri: "utilisateur/",
            method: ConstanteWs._CODE_PUT,
            params: {
                body: this.formUser.value
            }
        };

        this.sharedService.commonWs(request).subscribe({
            next: (response: ResponseObject) => {
                if (response.code == ConstanteWs._CODE_WS_SUCCESS) {
                    console.log(<[]>response.payload, ' <[]>response.payload tMicroservice/saveAll');
                    this.toast.success("Utilisateur modifié " + this.formUser.get('nom').value +"   "+  this.formUser.get('prenom').value + "  avec succès")
                    //this code to add a idCreatedMs attribute to the list of the microservices after post"   " +
                    this.router.navigate(['/app/adm/users'])

                } else {
                    console.error("Error in post microservice ")
                    ;
                    this.toast.error();
                }
            },
            error: (error) => {
                console.error("Error in post miscroservice");
                // localStorage.setItem('isMsCreated', 'false');
                this.toast.error();
            }
        });
    }


     async inituser(){
        const request: RequestObject = <RequestObject>{
            uri: "utilisateur/",
            params: {
                path: [this.id]

            },
            method: ConstanteWs._CODE_GET
        };
        return new Promise<void>((resolve, reject) => {
            this.sharedService.commonWs(request).subscribe({
                next: (response) => {
                    if (response.code == ConstanteWs._CODE_WS_SUCCESS) {

                        this.dataUser = response.payload;

                        resolve(this.dataUser);


                    } else {
                        console.error(
                            `Error in /initLangueProjet, error code :: ${response.code}`
                        );
                        this.toast.error();
                        reject();
                    }
                },
                error: (error) => {
                    console.error(
                        `Error in ColumnComponent/initLangueProjet, error :: ${error}`
                    );
                    this.toast.error();
                    reject();

                }
            });


        });


    }
}
